<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->integer('parent_id')->nullable();
            $table->integer('category_id')->nullable();            
            $table->integer('is_show_title');
            $table->string('name')->nullable();
            $table->string('seo_name')->nullable();
            $table->text('seo_body')->nullable();
            $table->text('seo_keyword')->nullable();
            $table->string('link')->nullable();
            $table->string('slug')->nullable();
            $table->string('type')->nullable();
            $table->integer('status')->nullable();
            $table->integer('language')->nullable();
            $table->string('images')->nullable();
            $table->float('order', 8, 2)->nullable();
            $table->text('short_body');
            $table->text('body')->nullable();
            $table->text('body_info')->nullable();
            $table->date('publish_at')->nullable();
            $table->date('unpublish_at')->nullable();
            $table->integer('featured')->nullable();
            $table->integer('count_page')->nullable();
            $table->integer('deleted')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
