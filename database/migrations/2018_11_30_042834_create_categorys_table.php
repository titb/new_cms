<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorys', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->integer('parent_id')->nullable();
            $table->string('name')->nullable();
            $table->integer('is_show_title');
            $table->string('link')->nullable();
            $table->string('slug')->nullable();
            $table->string('type')->nullable();
            $table->string('blog')->nullable();
            $table->integer('status')->nullable();
            $table->integer('language')->nullable();
            $table->string('images')->nullable();
            $table->float('order', 8, 2)->nullable();
            $table->text('body')->nullable();
            $table->date('publish_at')->nullable();
            $table->date('unpublish_at')->nullable();
            $table->integer('deleted')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorys');
    }
}
