<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->integer('menu_type_id')->index();
            $table->integer('parent_id')->nullable();
            $table->string('name')->nullable();
            $table->string('link')->nullable();
            $table->string('slug')->nullable();
            $table->string('type')->nullable();
            $table->integer('status')->nullable();
            $table->integer('language')->nullable();
            $table->text('description')->nullable();
            $table->string('images')->nullable();
            $table->float('order', 8, 2)->nullable();
            $table->date('publish_at')->nullable();
            $table->date('unpublish_at')->nullable();
            $table->string('color')->nullable();
            $table->string('icon_class')->nullable();
            $table->string('target')->nullable();
            $table->integer('lavel')->nullable();
            $table->integer('is_show_title');
            $table->integer('deleted')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
