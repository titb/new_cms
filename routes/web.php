<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Login

Route::get('login',['as'=>'login','uses'=>'LoginController@login']);
Route::post('login',['as'=>'login','uses'=>'LoginController@post_login']);
Route::get('logout',['as'=>'logout','uses'=>'LoginController@logout']);


Route::middleware(['auth'])->group(function () {
    // Admin
    Route::get('admin',['as'=>'admin','uses'=>'HomeController@index']);
    // Setting
    Route::get('setting/index',['as'=>'setting.index','uses'=>'SettingController@get_setting']);
    // Slide
    Route::get('slide/index',['as'=>'slide.index','uses'=>'SlideController@index_slide']);
    Route::get('slide/create',['as'=>'slide.create','uses'=>'SlideController@create_slide']);
    Route::post('slide/create',['as'=>'slide.create','uses'=>'SlideController@post_create_slide']);
    Route::get('slide/{id}',['as'=>'slide','uses'=>'SlideController@json_slide']);
    Route::delete('delete/slide/{id}',['as'=>'delete/slide/{id}','uses'=>'SlideController@deleted_slide']);
    // Video
    Route::get('video/index',['as'=>'video.index','uses'=>'VideoController@index_video']);
    Route::get('video/create',['as'=>'video.create','uses'=>'VideoController@create_video']);
    Route::post('video/create',['as'=>'video.create','uses'=>'VideoController@post_create_video']);
    Route::get('video/{id}',['as'=>'video','uses'=>'VideoController@json_video']);
    Route::delete('delete/video/{id}',['as'=>'delete/video/{id}','uses'=>'VideoController@deleted_video']);
    //Language
    Route::get('language/index',['as'=>'language.index','uses'=>'LanguageController@index_language']);
    Route::get('language/create',['as'=>'language.create','uses'=>'LanguageController@create_lang']);
    Route::post('language/create',['as'=>'language.create','uses'=>'LanguageController@post_create_lang']);
    Route::delete('language/delete/{id}',['as'=>'language/delete/{id}','uses'=>'LanguageController@delete_lang']);
    Route::get('language/{id}',['as'=>'language','uses'=>'LanguageController@lang_json']);
    //Social Media
    Route::get('social/index',['as'=>'social.index','uses'=>'SocialController@index_social', 'middleware'=>'permissions','permissions'=>['user_view','user_create','user_edit','user_show','user_delete']]);
    Route::get('social/create',['as'=>'social.create','uses'=>'SocialController@create_social']);
    Route::post('social/create',['as'=>'social.create','uses'=>'SocialController@post_create_social']);
    Route::delete('social/delete/{id}',['as'=>'social.delete','uses'=>'SocialController@deleted_social']);
    Route::get('social/{id}',['as'=>'social','uses'=>'SocialController@social_get_json']);
    Route::post('social/json',['as'=>'social/json','uses'=>'SocialController@index_social_json']); //Post Json to index
    // Categories
    Route::get('categories/index',['as'=>'categories/index','uses'=>'CategoryController@index_category']);
    Route::get('categories/create',['as'=>'categories.create','uses'=>'CategoryController@create_categories']);
    Route::post('categories/create',['as'=>'categories.create','uses'=>'CategoryController@post_create_categories']);
    Route::delete('categories/deleted/{id}',['as'=>'categories.deleted','uses'=>'CategoryController@delete_category']);
    Route::get('categories/{id}',['as'=>'categories','uses'=>'CategoryController@get_category_json']);
    // Posts
    Route::get('posts/index',['as'=>'posts/index','uses'=>'PostController@index_posts']);
    Route::get('posts/create',['as'=>'posts.create','uses'=>'PostController@create_posts']);
    Route::post('posts/create',['as'=>'posts.create','uses'=>'PostController@post_create_posts']);
    Route::delete('posts/del/{id}',['as'=>'posts.del','uses'=>'PostController@delete_posts']);
    Route::get('posts/{id}',['as'=>'posts','uses'=>'PostController@get_posts_json']);
    //menu types
    Route::get('menus/menu_types/index',['as'=>'menus.menu_types.index','uses'=>'MenuController@index_menu_type']);
    Route::get('menus/menu_types/create',['as'=>'menus.menu_types.create','uses'=>'MenuController@create_menu_type']);
    Route::post('menus/menu_types/create',['as'=>'menus.menu_types.create','uses'=>'MenuController@post_create_menu_type']);
    Route::delete('menus/menu_types/deleted/{id}',['as'=>'menus.menu_types.deleted','uses'=>'MenuController@deleted_menu_version']);
// menu type

// Menus        
    Route::get('menus/menus/index',['as'=>'menus.menus.index','uses'=>'MenuController@index_menus']);
    Route::get('menus/menus/create',['as'=>'menus.menus.create','uses'=>'MenuController@create_menus']);
    Route::get('menus/menus/{id}',['as'=>'menus.menus','uses'=>'MenuController@get_menu_json']);
    Route::post('menus/menus/create',['as'=>'menus.menus.create','uses'=>'MenuController@post_create_menu']);
    Route::delete('menus/menus/delete/{id}',['as'=>'menus/menus/delete/{id}','uses'=>'MenuController@delete_menu']);
// Menus 

// Media Show 
    Route::get('medias/create',['as'=>'medias.create','uses'=>'MadiasController@get_upload_create']);
    Route::get('medias/index',['as'=>'medias.index','uses'=>'MadiasController@index_media']);
    Route::get('medias/{directory}',['as'=>'medias','uses'=>'MadiasController@view_image_fild']);
    
    // Method
    Route::get('method/index',['as'=>'method/index','uses'=>'MethodController@get_index', 'middleware'=>'permissions','permissions'=>['method_view','method_create','method_edit','method_delete']]);
    Route::get('method/create',['as'=>'method/create','uses'=>'MethodController@get_create_method', 'middleware'=>'permissions','permissions'=>'method_create']);
    Route::post('method/create',['as'=>'method/create','uses'=>'MethodController@post_create_method', 'middleware'=>'permissions','permissions'=>'method_create']);
    Route::get('method/{id}/edit',['as'=>'method/{id}/edit','uses'=>'MethodController@get_edit_method', 'middleware'=>'permissions','permissions'=>'method_edit']);
    Route::post('method/{id}/edit',['as'=>'method/{id}/edit','uses'=>'MethodController@post_edit_method', 'middleware'=>'permissions','permissions'=>'method_edit']);
    Route::post('method/{id}/delete',['as'=>'method/{id}/delete','uses'=>'MethodController@deleted', 'middleware'=>'permissions','permissions'=>'method_delete']);
    // Menu
    Route::get('menus',['as'=>'menus','uses'=>'MenuController@index', 'middleware'=>'permissions','permissions'=>['menu_view','menu_create','menu_edit','menu_delete']]);
    // Permission
    Route::get('permission/index',['as'=>'permission/index','uses'=>'PermissionController@get_index', 'middleware'=>'permissions','permissions'=>['permission_view','permission_create','permission_edit','permission_delete']]);
    Route::get('permission/create',['as'=>'permission/create','uses'=>'PermissionController@get_create_permission', 'middleware'=>'permissions','permissions'=>'permission_create']);
    Route::post('permission/create',['as'=>'permission/create','uses'=>'PermissionController@post_create_permission', 'middleware'=>'permissions','permissions'=>'permission_create']);
    Route::get('permission/{id}/edit',['as'=>'permission/{id}/edit','uses'=>'PermissionController@get_edit_permission', 'middleware'=>'permissions','permissions'=>'permission_edit']);
    Route::post('permission/{id}/edit',['as'=>'permission/{id}/edit','uses'=>'PermissionController@post_edit_permission', 'middleware'=>'permissions','permissions'=>'permission_edit']);
    Route::post('permission/{id}/delete',['as'=>'permission/{id}/delete','uses'=>'PermissionController@deleted', 'middleware'=>'permissions','permissions'=>'permission_delete']);
    // Role
    Route::get('role/index',['as'=>'role/index','uses'=>'RoleController@get_index', 'middleware'=>'permissions','permissions'=>['role_view','role_create','role_edit','role_delete']]);
    Route::get('role/create',['as'=>'role/create','uses'=>'RoleController@get_create_role', 'middleware'=>'permissions','permissions'=>'role_create']);
    Route::post('role/create',['as'=>'role/create','uses'=>'RoleController@post_create_role', 'middleware'=>'permissions','permissions'=>'role_create']);
    Route::get('role/{id}/edit',['as'=>'role/{id}/edit','uses'=>'RoleController@get_edit_role', 'middleware'=>'permissions','permissions'=>'role_edit']);
    Route::post('role/{id}/edit',['as'=>'role/{id}/edit','uses'=>'RoleController@post_edit_role', 'middleware'=>'permissions','permissions'=>'role_edit']);
    Route::post('role/{id}/delete',['as'=>'role/{id}/delete','uses'=>'RoleController@deleted', 'middleware'=>'permissions','permissions'=>'role_delete']);
    // User
    Route::get('user/index',['as'=>'user/index','uses'=>'UserController@get_index', 'middleware'=>'permissions','permissions'=>['user_view','user_create','user_edit','user_show','user_delete']]);
    Route::get('user/create',['as'=>'user/create','uses'=>'UserController@get_create_user', 'middleware'=>'permissions','permissions'=>'user_create']);
    Route::post('user/create',['as'=>'user/create','uses'=>'UserController@post_create_user', 'middleware'=>'permissions','permissions'=>'user_create']);
    Route::get('user/{id}/edit',['as'=>'user/{id}/edit','uses'=>'UserController@get_edit_user', 'middleware'=>'permissions','permissions'=>'user_edit']);
    Route::post('user/{id}/edit',['as'=>'user/{id}/edit','uses'=>'UserController@post_edit_user', 'middleware'=>'permissions','permissions'=>'user_edit']);
    Route::post('user/{id}/delete',['as'=>'user/{id}/delete','uses'=>'UserController@deleted', 'middleware'=>'permissions','permissions'=>'user_delete']);
    Route::get('user/{id}/show',['as'=>'user/{id}/show','uses'=>'UserController@show_user', 'middleware'=>'permissions','permissions'=>'user_show']);
    Route::get('change/{id}/password',['as'=>'change/{id}/password','uses'=>'UserController@change_pass', 'middleware'=>'permissions','permissions'=>'user_edit']);
    Route::post('change/{id}/password',['as'=>'change/{id}/password','uses'=>'UserController@post_change_pass', 'middleware'=>'permissions','permissions'=>'user_edit']);
    Route::get('profile',['as'=>'profile','uses'=>'UserController@show_profile', 'middleware'=>'permissions','permissions'=>'user_show']);
    Route::get('change/{id}',['as'=>'change/{id}','uses'=>'UserController@admin_change_pass']);
    Route::post('change/{id}',['as'=>'change/{id}','uses'=>'UserController@post_admin_change_pass']);
    //product brand
    Route::get('product/brand/index',['as'=>'product/brand/index','uses'=>'ProductController@index_brand']);
    Route::get('product/brand/create',['as'=>'product/brand/create','uses'=>'ProductController@create_brand']);
    Route::post('product/brand/create',['as'=>'product/brand/create','uses'=>'ProductController@post_brand']);
    Route::get('product/brand/get_json',['as'=>'product/brand/get_json','uses'=>'ProductController@json_edit_brand']);
    Route::delete('delete/brand/{id}',['as'=>'delete/brand/{id}','uses'=>'ProductController@delete_brand']);

    Route::get('scan','TemplateController@scan_dir');
    Route::get('load_xml','TemplateController@load_xml');
    Route::get('modules/index','ModuleController@index_module');


        //Modules
    Route::get('module_index',['as'=>'module_index','uses'=>'ModuleController@module_index']);
    //create
    Route::get('module_get_create',['as'=>'module_get_create','uses'=>'ModuleController@module_get_create']);
    //post
    Route::post('post_create_module',['as'=>'post_create_module','uses'=>'ModuleController@post_create_module']);
    //edit
    Route::get('module_get_edit/{id}',['as'=>'module_get_edit/{id}','uses'=>'ModuleController@module_get_edit']);
    Route::get('module_get_val/{id}',['as'=>'module_get_val/{id}','uses'=>'ModuleController@module_post_edit']);
    //delete
    Route::post('module_post_delete/{id}',['as'=>'module_post_delete/{id}','uses'=>'ModuleController@module_post_delete']);

    // Route::get('posts_module/{id}',['as'=>'posts_module','uses'=>'ModuleController@get_posts_json']);






});
Route::get('/',function(){
    return redirect()->to('{link}');
});
Route::get('{link}',['as'=>'{link}','uses'=>'HomeController@template_index']);