<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <?php  
                        $user = App\User::get();
                        $role = App\Role::get();
                    ?>
                </div>
                <table id="example23" class="display nowrap table table-hover table-striped table-bordered dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example23_info" style="width: 100%;">
                            <thead>
                                <tr role="row">
                                    <th tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending"
                                        style="">#</th>
                                    <th tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending"
                                        style="">Name</th>
                                    <th tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" 
                                        style="">Role</th>
                                    <th tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" 
                                        style="">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($user as $key)
                                <form action="{{ url('user/'.$key->id.'/edit') }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                <tr role="row" class="odd">
                                    <td>{{ $key->id }}</td>
                                    <td>{{ $key->full_name }}</td>
                                    <td>
                                        @foreach($role as $val)
                                            {{ $val->display_name }} <input type="checkbox" name="role_id[]" value="{{ $val->id }}" <?php if(count($key->roles) > 0){ foreach($key->roles as $v){if($val->id == $v->id){echo "checked";}}}  ?>>
                                        @endforeach
                                    </td>
                                    <td>
                                        <button type="submit" class="btn btn-success btn-flat m-b-10 m-l-5">Submit </button>
                                    </td>
                                    
                                    
                                </tr>
                                </form>
                                @endforeach
                            </tbody>
                        </table>
            </div>
        </div>
    </body>
</html>
