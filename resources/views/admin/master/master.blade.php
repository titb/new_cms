<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('images/favicon.png') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <link href="{{ url('css/lib/chartist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/lib/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{ url('css/lib/nestable/nestable.css') }}" rel="stylesheet">
     <!-- Bootstrap Core CSS -->
     <link href="{{ url('css/lib/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
     <link href="{{ url('summernote/summernote.css') }}" rel="stylesheet">
     <link href="{{ url('gijgo/css/gijgo.css') }}" rel="stylesheet">
     <!-- Custom CSS -->
    <link href="{{ url('css/helper.css') }}" rel="stylesheet">
    <link href="{{ url('css/style.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    
@yield('css')
<script>
        function day_format_show(date_format){
            var d = new Date(date_format);

            var year_n = d.getFullYear();
            var month_n = d.getMonth() + 1;
            var day_n = d.getDate();
            // if(month_n > 10){
            //     month_n = month_n;
            // }else{
            //     month_n = "0"+month_n; 
            // }
            
            // if(day_n > 10){
            //     day_n = day_n;
            // }else{
            //     day_n = "0"+day_n; 
            // }

            return  day_n +"-"+month_n+"-"+year_n;
        }
</script>
</head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">

        <!-- header header  -->
        @include('admin.master.header')
        <!-- End header header -->

        <!-- Left Sidebar  -->
        @include('admin.master.sidebar')
        <!-- End Left Sidebar  -->

        <!-- Page wrapper  -->
        <div class="page-wrapper" style="min-height: 324px;">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary"></h3> </div>
                <div class="col-md-7 align-self-center ">
                    <!-- <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol> -->
                    @yield('bottom_header')
                   
                </div>
            </div>
            <div class="container-fluid">
                @include('error.error')
                @yield('content')
            </div>
            <!-- footer -->
            <!-- <footer class="footer"> © 2018 All rights reserved. Create By by <a href="https://titb.biz">TITB</a></footer> -->
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="{{ url('js/lib/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('gijgo/js/gijgo.min.js') }}"></script>

    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ url('js/lib/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ url('js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ url('js/jquery.slimscroll.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ url('js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ url('js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ url('summernote/summernote.js') }}"></script>

 

    <!-- Amchart -->
    <!-- <script src="{{ url('js/lib/morris-chart/raphael-min.js') }}"></script>
    <script src="{{ url('js/lib/morris-chart/morris.js') }}"></script> -->
    <!-- <script src="{{ url('js/lib/morris-chart/dashboard1-init.js') }}"></script> -->
    

	<!-- <script src="{{ url('js/lib/calendar-2/moment.latest.min.js') }}"></script> -->
    <!-- scripit init-->
    <!-- <script src="{{ url('js/lib/calendar-2/semantic.ui.min.js') }}"></script> -->
    <!-- scripit init-->
    <!-- <script src="{{ url('js/lib/calendar-2/prism.min.js') }}"></script> -->
    <!-- scripit init-->
    <!-- <script src="{{ url('js/lib/calendar-2/pignose.calendar.min.js') }}"></script> -->
    <!-- scripit init-->
    <!-- <script src="{{ url('js/lib/calendar-2/pignose.init.js') }}"></script> -->

    <script src="{{ url('js/lib/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ url('js/lib/owl-carousel/owl.carousel-init.js') }}"></script>
    <!-- <script src="{{ url('js/scripts.js') }}"></script> -->


    @yield('script')
    <script src="{{ url('js/custom.min.js') }}"></script>
    <script>
          
        $("#setecte_all").change(function(){ 
            var status = this.checked; 
            $('.setecte_id').each(function(){ 
                this.checked = status;
            });
            if(status == true){
                $(".remove_all").removeAttr('style');
            }else{
                $(".remove_all").css('display','none');
            }
            
        });

        $('.setecte_id').change(function(){ 
            
            if(this.checked == false){ 
                $("#setecte_all")[0].checked = false;
                $(".remove_all").css('display','none');
            }
            
            if ($('.setecte_id:checked').length == $('.setecte_id').length ){ 
                $("#setecte_all")[0].checked = true; 
                $(".remove_all").removeAttr('style');
            }
            
        });
        
        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: true                  // set focus to editable area after initializing summernote
        });

        $('.datepicker').datepicker({ format: 'dd-mm-yyyy' });
        $('.datepicker1').datepicker({ format: 'dd-mm-yyyy' });
    </script>
</body>

</html>