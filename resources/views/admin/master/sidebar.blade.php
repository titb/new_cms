<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <!-- <li class="nav-label">Home</li> -->
                <li> <a  href="{{url('admin')}}"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span> </a></li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-list"></i> <span class="hide-menu">Menus</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('menus/menus/index')}}">Menu</a></li>
                        <li><a href="{{url('menus/menu_types/index')}}">MenuType</a></li>
                    </ul>
                </li>
                <li> <a  href="{{url('module_index')}}"><i class="fa fa-tachometer"></i><span class="hide-menu">Module</span> </a></li>
                <li> <a  href="{{url('categories/index')}}"><i class="fa fa-tachometer"></i><span class="hide-menu">Category</span> </a></li>
                <li> <a  href="{{url('posts/index')}}"><i class="fa fa-tachometer"></i><span class="hide-menu">Post</span> </a></li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-list"></i> <span class="hide-menu">Product Menegement</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('product/brand/index')}}">Brand</a></li>
                        <li><a href="{{url('product/category/index')}}">Category</a></li>
                    </ul>
                </li>
                <li> <a  href="{{url('medias/index')}}"><i class="fa fa-photo"></i><span class="hide-menu">Media</span> </a></li>
                <li> <a  href="{{url('language/index')}}"><i class="fas fa-language"></i><span class="hide-menu">Language</span> </a></li>
                <li> <a  href="{{url('slide/index')}}"><i class="fa fa-sliders"></i><span class="hide-menu">Slide</span> </a></li>
                <li> <a  href="{{url('social/index')}}"><i class="fab fa-facebook-square"></i><span class="hide-menu">Social Media</span> </a></li>	
                <li> <a  href="{{url('video/index')}}"><i class="fas fa-video"></i><span class="hide-menu">Video</span> </a></li>	
                <!-- <li class="nav-label">User Management</li> -->
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu">User Management</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('user/index')}}">User</a></li>
                        <li><a href="{{url('role/index')}}">Role</a></li>
                        <li><a href="{{url('permission/index')}}">Permission</a></li>
                        <li><a href="{{url('method/index')}}">Method</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</div>