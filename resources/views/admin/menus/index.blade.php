@extends('admin.master.master')
@section('title','Menu List')
@section('bottom_header')
    <a href="{{ url('menus/menus/create?redirect=menus&item_id=0') }}" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add New </a>
@endsection
<style>
.dd .item_actions {
    z-index: 9;
    position: absolute;
    top: 6px;
    right: 10px;
}
.dd .item_actions .btn {
    margin: 0 4px;
}
</style>
@section('content')
  <div class="rows">
        <!-- /# column -->
        <div class="card">
           
            <div class="card-body">
                <div class="card-content">
                    <div class="nestable">
                        <div class="dd" id="nestable">
                            <ol class="dd-list" ondrop="drop(event)" ondragover="allowDrop(event)">
                                @foreach($data as $key => $d)
                                    <li draggable="true" ondragstart="drag(event)" class="dd-item dd3-item" id="{{ $key+1 }}" data-id="{{$key + 1}}">
                                        <div class="dd-handle dd3-handle"></div>
                                        <div class="dd3-content">{{$d->name}}</div>
                                        <div class="pull-right item_actions">
                                            <div class="btn btn-sm btn-danger btn-rounded pull-right delete" data-id="{{$d->id}}">
                                                <a href="{{ url('menus/menus/delete/'.$d->id) }}"><i class="fa fa-trash"></i> Delete</a>
                                            </div>
                                            <a href="{{ url('menus/menus/create?redirect=menus&item_id='.$d->id.'') }}" class="btn btn-sm btn-primary btn-rounded pull-right edit" >  <i class="fa fa-edit"></i> Edit</a>
                                        </div> 
                                    </li>
                                @endforeach
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript">
    // $( ".dd-list" ).sortable({
    //     // delay: 150,
    //     stop: function() {
    //         var selectedData = new Array();
    //         $('.dd-list>li').each(function() {
    //             selectedData.push($(this).attr("id"));
    //         });
    //         updateOrder(selectedData);
    //     }
    // });
    // function drop(event) {
    //   event.preventDefault();
    //    var o = document.getElementById("m1");
    //    var m = o.offset();
    //    console.log(m.top);
    // }
    // var cl = $(".dd-item:last");
    // var clf = $(".dd-item:first");
    // var offset = cl.offset();
    // var offsetd = clf.offset();
    // console.log(offset.top);
    // console.log(offsetd.top);
    // function allowDrop(ev) {
    //   ev.preventDefault();
    // }
    // function drop(ev) {
    //     ev.preventDefault();
        
        // var m = $(".dd-item:last").attr("id");
        // var text = $(".dd-item").attr("id");
        // var i;
        // for (i = 2; i <= m; i++) {
        //     text += [i];
        // }
        // console.log(text);
    // }
    
    
     

    
</script>

  </div>
    @section('script') 
        <!-- scripit init-->
        <script src="{{ url('js/lib/nestable/jquery.nestable.js') }}"></script>
        <!-- scripit init-->
        <script src="{{ url('js/lib/nestable/nestable.init.js') }}"></script>
    @endsection    
@endsection