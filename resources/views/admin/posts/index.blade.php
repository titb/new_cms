@extends('admin.master.master')
@section('title','Post List')

@section('bottom_header')  
    <button class="btn btn-danger pull-right remove_all"  style="display:none"><i class="fa fa-remove"></i> Delete </button>

    <a href="{{url('posts/create?redirect=posts&item_id=0')}}" class="btn btn-success pull-right" style=" margin-right: 10px;" ><i class="fa fa-plus"></i> Add New </a> 
 @endsection
@section('content')
  <div class="rows">
        <!-- /# column -->
        <div class="card">
           
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th><input id="setecte_all"  class="setecte_all" type="checkbox">  ID</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Status</th>
                                <th>Language</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $d)
                            <tr>
                                <th scope="row"><input id="setecte_id" name="setecte_id[]"  class="setecte_id" type="checkbox" value="{{$d->id}}"> {{$d->id}}</th>
                                <td>{{$d->name}}</td>
                                <td>
                                    @if($d->category_id)
                                        {{ $d->category->name }}
                                    @else   
                                        Uncategories
                                    @endif
                                </td>
                                <td>
                                    @if($d->status == 1)
                                        ACTIVE
                                    @else
                                        INACTIVE
                                    @endif
                                </td>
                                <td>Language</td>
                                <td>
                                    <a href="{{url('posts/create?redirect=posts&item_id='.$d->id.'')}}" class="btn btn-info btn-rounded m-b-10 m-l-5 edit" ><i class="fa fa-edit"></i> Edit</a>
                                     <form action="{{ route('posts.del', ['id' => $d->id]) }}"  id="delete_form" method="POST" style=' display: inline-block;'>
                                        {{ method_field("DELETE") }}
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-danger btn-rounded m-b-10 m-l-5 delete_confirm"><i class="fa fa-remove"></i> Delete</button> 
                                     </form> 
                                      
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

  </div> 

  @section('script')
       <!-- scripit init-->
       <script src="{{ url('js/lib/nestable/jquery.nestable.js') }}"></script>
    <!-- scripit init-->
    <script src="{{ url('js/lib/nestable/nestable.init.js') }}"></script>
    <script>
        $(".delete_confirm").click(function(){
            var r = confirm("Are You Sure . Do you want to Delete This Row");
            if (r == true) {
              $("#delete_form").submit();
            } else {
               return false;
            }
        });
    </script>
  @endsection
@endsection