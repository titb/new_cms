@extends('admin.master.master')
@section('title','Post List')

@section('bottom_header')
    <a href="{{url('product/brand/create?redirect=brand&item_id=0')}}" class="btn btn-success pull-right" ><i class="fa fa-plus"></i> Add New </a>
@endsection
@section('content')
  <div class="rows">
        <!-- /# column -->
        <div class="card">
           
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="10%">Image</th>
                                <th>Name</th>
                                <th>code</th>
                                <th>Category</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($brand as $val)
                                <tr>
                                    <td><img src="{{$val->images}}" width="50px" height="50px"></td>
                                    <td>{{$val->name}}</td>
                                    <td>{{$val->code}}</td>
                                    <td>{{$val->product_category->name}}</td>
                                    <td>{!! $val->description !!}</td>
                                    <td>
                                        <a href="{{url('product/brand/create?redirect=brand&item_id='.$val->id.'')}}" class="btn btn-info btn-rounded m-b-10 m-l-5 edit" ><i class="fa fa-edit"></i> Edit</a>
                                        <form action="{{ url('delete/brand/'.$val->id) }}"  id="delete_form" method="POST" style=' display: inline-block;'>
                                            {{ method_field("DELETE") }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-rounded m-b-10 m-l-5 delete_confirm"><i class="fa fa-remove"></i> Delete</button> 
                                        </form> 
                                        
                                    </td>
                                    
                                    
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

  </div> 
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @section('script')
       <!-- scripit init-->
       <!-- <script src="{{ url('js/lib/nestable/jquery.nestable.js') }}"></script> -->
    <!-- scripit init-->
    <!-- <script src="{{ url('js/lib/nestable/nestable.init.js') }}"></script> -->
    <script>
        $(".delete_confirm").click(function(){
            var r = confirm("Are You Sure . Do you want to Delete This Row");
            if (r == true) {
              $("#delete_form").submit();
            } else {
               return false;
            }
        });
        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });
        // $.ajax({
        //     type: 'POST',
        //     url: '{{route("social/json")}}',
        //     dataType: 'json',
        //     contentType: false,
        //     processData: false,
        //     success: function(dat){
        //         console.log(dat);
        //     }
        // });
    </script>
  @endsection
@endsection