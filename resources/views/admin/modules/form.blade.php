@extends('admin.master.master')
@section('title','Menu Type List')

@section('bottom_header')
<a href="{{url('module_index')}}" class="btn btn-info pull-right"><i class="fa fa-back"></i> View List </a>
@endsection
@section('content')
<style>
   .accord-card {
      margin: 0;
      padding: 0;
   }
</style>
<div class="rows">
   <!-- /# column -->
   <div class="col-md-12">
      <div class="card">
         <div class="card-body p-b-0">
            <!-- <h4 class="card-title">Create Menu</h4> -->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs customtab" role="tablist">
               <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home2" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Detail</span></a> </li>
               <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile2" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Menu</span></a> </li>
               <!-- <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages2" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Messages</span></a> </li> -->
            </ul>
            <!-- Tab panes -->
            <form action="{{url('post_create_module')}}" class="form form_module p-t-20 form_menu_type" id="form_module" name="form_module" method="POST" enctype="multipart/form-data">
               {{ csrf_field() }}
               <div class="tab-content">
                  <div class="tab-pane active" id="home2" role="tabpanel">
                     <div class="p-20">
                        <div class="col-lg-6 pull-left">
                           <div class="card">
                              <div class="card-body">

                                 <div class="form-group">
                                    <label for="name">Title</label>
                                    <div class="input-group">
                                       <input type="text" class="form-control title" name="title" id="titles" placeholder="title">
                                    </div>
                                    <span for="name" class="text-danger val_name" style="display: none;">title Name is a required field</span>
                                 </div>

                                 <div class="form-group">
                                    <label for="name">ordering</label>
                                    <div class="input-group">
                                       <input type="text" class="form-control name ordering" name="ordering" id="orderings" placeholder="">
                                    </div>
                                    <span for="name" class="text-danger val_name" style="display: none;">ordering is a required field</span>
                                 </div>

                                 <div class="form-group">
                                    <label>Possition</label>
                                    <select class="form-control input-flat position" id="positions" name="position" value="">
                                       <option value="">Select Possition</option>
                                       <?php
                                       $path = resource_path('views\templates\templateDetails.xml');
                                       $xml = simplexml_load_file($path);
                                       $data = $xml->positions;
                                       foreach ($data as $showpossition) {
                                          foreach ($showpossition->position as $possiotion) {
                                             echo '<option value="' . $possiotion . '">' . $possiotion . '<br></option>';
                                          }
                                       }
                                       ?>
                                    </select>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6 pull-right">
                           <div class="card">
                              <div class="card-body">
                                 <!-- <h4 class="card-title"></h4> -->
                                 <!-- <div class="card-content"> -->
                                 <div class="form-group">
                                    <label> Module</label>
                                    <select class="form-control input-flat module" id="modules" name="module">
                                       <option value="">Select Style</option>
                                       <?php
                                       $path = resource_path('views\templates\templateDetails.xml');
                                       $xml = simplexml_load_file($path);
                                       $data = $xml->styles;
                                       foreach ($data as $showstyle) {
                                          foreach ($showstyle->style as $style) {
                                             echo '<option value="' . $style . '">' . $style . '<br></option>';
                                          }
                                       }
                                       ?>
                                    </select>
                                 </div>
                                 <div class="form-group">
                                    <label> <?php echo Request()->module_type=='post'?'Post':'Category'; ?></label>
                                    <select class="form-control input-flat post_id" id="post_id" name="post_id">
                                       <option></option>
                                       <?php
                                       $md_type_data = Request()->module_type=='post'?App\Post::get():App\Category::get();
                                       foreach ($md_type_data as $pd) {
                                       ?>
                                          <option value="<?php echo $pd->id; ?>"><?php echo $pd->name; ?>
                                          </option>
                                       <?php } ?>
                                    </select>
                                 </div>
                                 <div class="form-group">
                                    <label> Language</label>
                                    <select class="form-control input-flat language" id="languages" name="language">
                                       <option value="English">English</option>
                                       <option value="khmer">khmer</option>
                                    </select>
                                 </div>

                                 <div class="form-group">
                                    <label for="">show title</label>
                                    <label class="switch">
                                       <input class="switch-input show_title" type="checkbox" id="switch_check" name="show_title" value="1" checked>
                                       <span class="switch-label" data-on="On" data-off="Off"></span>
                                       <span class="switch-handle"></span>
                                    </label>

                                 </div>
                                 <!-- </div> -->
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php $menu = App\Menu::all(); ?>
                  <div class="tab-pane  p-20" id="profile2" role="tabpanel">
                     <div class="col-lg-5 pull-left">
                        <div class="card">
                           <div class="card-body">
                              <div id="accordion">
                                 <div class="card accord-card">
                                    <a class="card-link" data-toggle="collapse" href="#collapseOne">
                                       <div class="card-header">
                                          Post
                                       </div>
                                    </a>
                                    <div id="collapseOne" class="collapse show" data-parent="#accordion">
                                       <div class="card-body">
                                          <ul class="nav nav-tabs" role="tablist">
                                             <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Home</span></a> </li>
                                             <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Profile</span></a> </li>
                                             <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Messages</span></a> </li>
                                          </ul>
                                          <div class="tab-content tabcontent-border">
                                             <div class="tab-pane active" id="home" role="tabpanel">
                                                <div class="p-20">
                                                   <h5>Best Clean Tab ever</h5>
                                                   <h6>you can use it with the small code</h6>
                                                   <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                                                </div>
                                             </div>
                                             <div class="tab-pane  p-20" id="profile" role="tabpanel">2</div>
                                             <div class="tab-pane p-20" id="messages" role="tabpanel">3</div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="card accord-card">
                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                       <div class="card-header">
                                          Category
                                       </div>
                                    </a>
                                    <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                       <div class="card-body">
                                          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-7 pull-right">
                        <div class="card">
                           <div class="card-body">
                              <div class="table-responsive">
                                 <table class="table table-bordered">

                                    <div class="form-group">
                                       <label>Select Menu :</label>
                                       <select class="form-control input-flat show_menu_type" id="mySelect" name="show_menu_type" value="">
                                          <option value=""></option>
                                          <option value="0" id="">select all Menu</option>
                                          <option value="1">select by Menu</option>
                                       </select>
                                    </div>
                                    <div class="dis" style="display:none;">
                                       <p id="demo">
                                          <label>show all menu:</label>
                                          @foreach($menu as $mn)
                                          <div class="col-md-12 all_menu" id="all_menu">
                                             <input class="check_id" type="checkbox" name="check[]
                                                " id="check_ed" value="{{$mn->id}}">&nbsp;&nbsp;{{$mn->name}}
                                          </div>
                                          @endforeach
                                       </p>
                                    </div>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="tab-pane p-20" id="messages2" role="tabpanel">3</div> -->
               </div>
               <div class="text-left">
                  <button type="button" name="btn_submit" id="submitf" class="submit_button floating-button btn btn-info" value="{{$module_edit_id}}">Submit</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<meta name="_token" content="{{ csrf_token() }}" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

@section('script')
<!-- post data  -->
<script>
   $(document).ready(function() {
      $(".submit_button").click(function() {
         var url = "{{url('post_create_module')}}";
         $.ajaxSetup({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
         });
         var form = document.forms.namedItem("form_module");
         var formDatas = new FormData(form);

         var title = $("#titles").val();
         var ordering = $('#orderings').val();
         var position = $('#positions').val();
         var modules = $('#modules').val();
         var language = $('#languages').val();
         var show_menu_type = $('#mySelect').val();
         var show_title = $('#switch_check').val();
         var post_id = $('#post_id').val();
         var check = [];
         $('.check_id:checked').each(function(i, e) {
            check.push($(this).val());
         });

         // console.log(title+ordering+position+modules+language+show_menu_type+show_title+check); 
         $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {
               'btn_submit': $(this).val(),
               'title': title,
               'show_menu_type': show_menu_type,
               'ordering': ordering,
               'position': position,
               'module': modules,
               'show_title': show_title,
               'language': language,
               'check': check,
               'post_id': post_id,
               'module_type' : '{{Request()->module_type}}'
            },

            success: function(data) {
               console.log(data);
               location.href = "{{url('module_get_create')}}";
            },
            error: function(data) {
               console.log('Error:', data);
            }
         });
      });
   });
</script>
@endsection
<!-- get_show_edit -->
<script>
   $(document).ready(function() {

      if ('{{$module_edit_id}}' != 0) {
         $.ajax({
            url: "{{url('module_get_val')}}" + "/{{$module_edit_id}}",
            success: function(data) {
               console.log(data);
               $('#titles').val(data.title);
               $('#orderings').val(data.ordering);
               $('#positions').val(data.position);
               $('#module').val(data.module);
               $('#languages').val(data.language);
               //for show title 
               if (data.show_title == '0') {
                  $('#switch_check').val(data.show_title);
                  $('#switch_check').removeAttr("checked", "checked");
               } else if (data.show_title == '1') {
                  $('#switch_check').val(data.show_title);
                  $('#switch_check').attr("checked", "checked");
               }
               // $('#switch_check').val(data.show_title);
               //for show show menu type   
               if (data.show_menu_type === '0') {
                  $('#mySelect').val(data.show_menu_type);
                  $('.dis').css({
                     "display": "none"
                  });
                  $('.check_id').attr("checked", "checked");
               } else if (data.show_menu_type === '1') {
                  $('#mySelect').val(data.show_menu_type);
                  $('.dis').css({
                     "display": "block"
                  });
                  // $("#check_ed").prop("checked", true);
                  $.each(data.menus, function(k, p) {
                     $(".check_id[value='" + p.id + "']").prop('checked', true);
                     // console.log($(".check_id[value='"+p.id+"']").val());
                  });
               }
            },
            error: function(error) {
               console.log(error);
            },
         });
      }
   });
</script>

<!-- button on off for text show_title -->
<script>
   // $("#switch_check").on('change', function() {
   //    var value = $("#switch_check").val(); 
   // if (value === '0'){
   //    $('#switch_check').attr('value', '1');
   // }else if(value === '1'){
   //    $('#switch_check').attr('value', '0');
   // }});

   $("#switch_check").on('change', function() {
      if ($(this).is(':checked')) {
         $(this).attr('value', '1');
         $(this).attr("checked", "checked");
      } else if ($(this).val('" "')) {
         $(this).attr('value', '0');
         $(this).removeAttr("checked", "checked");
      }
   });
</script>

<!-- for style select  -->
<script>
   $("#mySelect").on('change', function() {
      if ($(this).val() === '0') {
         $('#demo').hide();
         $('.all_menu').hide();
         $('.check_id').attr("checked", "checked");
         // $('#switch_check').removeAttr("checked", "checked");

      } else if ($(this).val() === '') {
         $('#demo').hide();
         $('.all_menu').hide();
      } else {
         $('#demo').show();
         $('.all_menu').show();
         $('.check_id').removeAttr("checked", "checked");
         $('.dis').css({
            "display": "block"
         });
         // $('#switch_check').attr("checked", "checked");

      }
   });
</script>

@endsection
<style>
   .all_menu {
      display: none;
      display: inline;
      line-height: 35px;
      font-size: 20px;
   }

   .switch {
      position: relative;
      display: inline-block;
      width: 60px;
      height: 34px;
   }

   .switch input {
      opacity: 0;
      width: 0;
      height: 0;
   }

   .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
   }

   .slider:before {
      position: absolute;
      content: "";
      height: 26px;
      width: 26px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
   }

   input:checked+.slider {
      background-color: #2196F3;

   }


   input:checked+.slider:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
   }

   /* Rounded sliders */
   .slider.round {
      border-radius: 34px;
   }

   .slider.round:before {
      border-radius: 50%;
   }
</style>
<style>
   .switch {
      position: relative;
      display: block;
      vertical-align: top;
      width: 100px;
      height: 30px;
      padding: 3px;
      margin: 0 10px 10px 0;
      background: linear-gradient(to bottom, #eeeeee, #FFFFFF 25px);
      background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF 25px);
      border-radius: 18px;
      box-shadow: inset 0 -1px white, inset 0 1px 1px rgba(0, 0, 0, 0.05);
      cursor: pointer;
      box-sizing: content-box;
   }

   .switch-input {
      position: absolute;
      top: 0;
      left: 0;
      opacity: 0;
      box-sizing: content-box;
   }

   .switch-label {
      position: relative;
      display: block;
      height: inherit;
      font-size: 10px;
      text-transform: uppercase;
      background: #eceeef;
      border-radius: inherit;
      box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);
      box-sizing: content-box;
   }

   .switch-label:before,
   .switch-label:after {
      position: absolute;
      top: 50%;
      margin-top: -.5em;
      line-height: 1;
      -webkit-transition: inherit;
      -moz-transition: inherit;
      -o-transition: inherit;
      transition: inherit;
      box-sizing: content-box;
   }

   .switch-label:before {
      content: attr(data-off);
      right: 11px;
      color: #aaaaaa;
      text-shadow: 0 1px rgba(255, 255, 255, 0.5);
   }

   .switch-label:after {
      content: attr(data-on);
      left: 11px;
      color: #FFFFFF;
      text-shadow: 0 1px rgba(0, 0, 0, 0.2);
      opacity: 0;
   }

   .switch-input:checked~.switch-label {
      background: blue;
      box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);
   }

   .switch-input:checked~.switch-label:before {
      opacity: 0;
   }

   .switch-input:checked~.switch-label:after {
      opacity: 1;
   }

   .switch-handle {
      position: absolute;
      top: 4px;
      left: 4px;
      width: 28px;
      height: 28px;
      background: linear-gradient(to bottom, #FFFFFF 40%, #f0f0f0);
      background-image: -webkit-linear-gradient(top, #FFFFFF 40%, #f0f0f0);
      border-radius: 100%;
      box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);
   }

   .switch-handle:before {
      content: "";
      position: absolute;
      top: 50%;
      left: 50%;
      margin: -6px 0 0 -6px;
      width: 12px;
      height: 12px;
      background: linear-gradient(to bottom, #eeeeee, #FFFFFF);
      background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF);
      border-radius: 6px;
      box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);
   }

   .switch-input:checked~.switch-handle {
      left: 74px;
      box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);
   }

   /* Transition
   ========================== */
   .switch-label,
   .switch-handle {
      transition: All 0.3s ease;
      -webkit-transition: All 0.3s ease;
      -moz-transition: All 0.3s ease;
      -o-transition: All 0.3s ease;
   }
</style>