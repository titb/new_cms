@extends('admin.master.master')
@section('title','Menu Type List')

@section('bottom_header')
<button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#selectModuleType">
    <i class="fa fa-plus"></i> Add New 
</button>
<!-- <a href="{{url('module_get_create')}}" class="btn btn-success pull-right"></a> -->
@endsection
@section('content')
<!-- <link href="{{url('mdb/css/bootstrap.min.css')}}" rel="stylesheet"> -->
<!-- Material Design Bootstrap -->
<!-- <link href="{{url('mdb/css/mdb.min.css')}}" rel="stylesheet"> -->
<!-- Your custom styles (optional) -->
<!-- <link href="{{url('mdb/css/style.css')}}" rel="stylesheet"> -->
<div class="rows">
    <!-- /# column -->
    <div class="card">

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th><input id="setecte_all" class="setecte_all" type="checkbox"> ID</th>
                            <th>User Name</th>
                            <th>Title</th>
                            <th>Ordering</th>
                            <th>possition</th>
                            <th>Module</th>
                            <th>Show title</th>
                            <th>Language</th>
                            <th style="text-align:center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($module)>0)
                        @foreach($module as $key=>$md)
                        <tr>
                            <th scope="row"><input id="setecte_id" class="setecte_id" type="checkbox" value="{{$md->id}}"> {{$key+1}}</th>
                            <th>{{Auth::user()->username}}</th>
                            <td>{{$md->title}}</td>
                            <td>{{$md->ordering}}</td>
                            <td>{{$md->position}}</td>
                            <td>{{$md->module}}</td>
                            <td style="text-align:center;">
                                @if($md->show_title ==1)
                                <span style="background:#377E8A;padding:3px;border-radius:20px;padding-right:15px;padding-left:15px;color:white;">show</span>
                                @else
                                <span style="background:#732C1B;padding:3px;border-radius:20px;padding-right:15px;padding-left:15px;color:white;">hide</span>
                                @endif
                            </td>
                            <td>{{$md->language}}</td>
                            <td style="width:250px;">
                                <a href="{{url('module_get_edit/'.$md->id)}}" class="btn btn-info btn-rounded m-b-10 m-l-5 edit"><i class="fa fa-edit"></i> Edit</a>
                                <form action="{{url('module_post_delete/'.$md->id)}}" id="delete_form" method="POST" style=' display: inline-block;'>
                                    {{ method_field("POST") }}
                                    {{ csrf_field() }}
                                </form>
                                <button type="button" class="btn btn-danger btn-rounded m-b-10 m-l-5 delete_confirm"><i class="fa fa-remove"></i> Delete</button>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="selectModuleType" tabindex="-1" role="dialog" aria-labelledby="selectModuleTypeLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Select Module Type</h5>
            </div>
            <div class="modal-body">
                <ul>
                    <li><a href="{{url('module_get_create?module_type=post')}}">Post</a></li>
                    <li><a href="{{url('module_get_create?module_type=category')}}">Category</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

@section('script')
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="{{url('mdb/js/popper.min.js')}}"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{url('mdb/js/bootstrap.min.js')}}"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{url('mdb/js/mdb.min.js')}}"></script>
<!-- scripit init-->
<script src="{{ url('js/lib/nestable/jquery.nestable.js') }}"></script>
<!-- scripit init-->
<script src="{{ url('js/lib/nestable/nestable.init.js') }}"></script>
<script>
    $(".delete_confirm").click(function() {
        var r = confirm("Are You Sure . Do you want to Delete This Row");
        if (r == true) {
            $("#delete_form").submit();
        } else {
            return false;
        }
    });
</script>
@endsection

@endsection