@extends('admin.master.master')
@section('title','Post List')

@section('bottom_header')
    <a href="{{url('social/index')}}" class="btn btn-info pull-right" > View List </a>
@endsection
@section('content')
  <div class="rows">
        <!-- /# column -->
    <form class="form form_posts" id="form_social" name="form_social" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="col-lg-8 pull-left">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Name <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control name" name="name" id="name"  >
                            
                        </div>
                        <span for="name" class="text-danger val_name" style="display: none;">Name is a required field</span>
                    </div>
                    <div class="form-group">
                        <label for="icon_class">Icon Class</label>
                        <div class="input-group">
                            <input type="text" class="form-control icon_class"  name="icon_class" id="icon_class" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control status" id="status" name="status">
                                <option value="1">ACTIVE</option>
                                <option value="0">INACTIVE</option>
                        </select>
                    </div>
                    <!-- <div class="form-group">
                        <label for="description">Description</label>
                        <textarea id="summernote" class="summernote form-control description" name="description"   placeholder="Enter text ..." ><p id="des"></p></textarea>
                    </div> -->
                    <div class="form-group">
                        <label for="image"> Image</label>
                        <div class="input-group">
                            <input type="file" class="form-control images" name="images" id="images" placeholder="Upload Image" >
                            
                        </div>
                        <p class="show_image"></p>
                            <input type="hidden" name="images1" value="" id="images1">
                    </div>
                    <div class="text-left">
                        <!-- <input type="button" name="submitf" value="Save" id="submitf" class="submit_button floating-button btn btn-info">  -->
                        <button type="button" name="submitf" class="submit_button floating-button btn btn-info" value="{{$item_id}}">Submit</button>
                        <!-- <button type="reset" class="btn btn-inverse waves-effect waves-light">Cancel</button> -->
                    </div>
                </div>
            </div>
        </div>
    </form>
  </div>
  <meta name="_token" content="{{ csrf_token() }}" />
@section('script')
<script>
    var submitting = false;
    function convertSerializedArrayToHash(a) { 
        var r = {};  
        for (var i = 0;i<a.length;i++) {
                if(a[i].name !== 'submitf')
                {
                    r[a[i].name] += a[i].value;
                }
        }
        return r;
    }

 var $form = $('#form_social').eq(0);
// alert($form.serializeArray());
var startItems;
$(document).ready(function() {		
	if(!startItems)
	{
        startItems = convertSerializedArrayToHash($form.serializeArray());
        // alert(startItems);
	}
});
function validate(formdata){
    if(formdata.name != ""){
        $(".val_name").css('display','none');
        return true; 
    }else{
       $(".val_name").removeAttr('style');    
       return false; 
    }
}
$(".name").keyup(function(){
   var name =  $(this).val();
   var formdata = {
        name :name
    }
    validate(formdata);
});
// Function Edit
var item_id = $(".submit_button").val();
if(item_id != 0){
    get_social_show(item_id);
}

function get_social_show(item_id){
    var url_e = "{{url('social')}}/"+item_id;
    $.get(url_e, function (data) {
        console.log(data);

        $(".name").val(data.name);
        $(".link").val(data.link);
        $(".icon_class").val(data.class_icon);

        // $(".color").val(data.color);
        // $(".icon_class").val(data.icon_class);
        // $('.menu_type_id option[value="'+data.menu_type_id+'"]').prop('selected', true);
        // $('.category_id option[value="'+data.parent_id+'"]').prop('selected', true);
        // $('.language option[value="'+data.language+'"]').prop('selected', true);
        // var order = Math.round(data.order);
        // $('.order option[value="'+order+'"]').prop('selected', true);
        
        // if(data.publish_at != null){
        //     $('.publish_at').val(day_format_show(data.publish_at));
        // }
        // if(data.unpublish_at != null){
        //     $('.unpublish_at').val(day_format_show(data.publish_at));
        // }

        $('.status option[value="'+data.status+'"]').prop('selected', true);
        if(data.images != null){
            var show_image = '<br/><img src="'+data.images+'" width="100px" height="100px">';
            $(".show_image").html(show_image);
            $("#images1").val(data.images);
        }
    });
}
$(".submit_button").click(function(){
    var url = "{!! url()->full() !!}";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }

    });
    var item_id = $(this).val();
    // if(item_id == 0){
    //     var url = "";
    // }

    var name = $(".name").val();
    // var description = $(".description").val();
    
    var formdata = {
        name :name,
        // description: description 
    }
    validate(formdata);

    if(validate(formdata) == true){
       
        var form = document.forms.namedItem("form_social");
        var formDatas = new FormData(form);   
        $.ajax({
            type:'POST', 
            url: url,
            dataType: 'json',
            contentType: false,
            data: formDatas,
            processData: false,
            success: function (result) {
                console.log(result);
                if(item_id == 0){
                    location.reload();
                }else{
                    location.href= "{{url('social/index')}}";
                }
            },
            error: function (result) {
                console.log('Error:', result);
            }
        });


    }
});


</script>  
@endsection
@endsection