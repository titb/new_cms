@extends('admin.master.master')
@section('title','Post List')

@section('bottom_header')
    <a href="{{url('slide/index')}}" class="btn btn-info pull-right" > View List </a>
@endsection
@section('content')
  <div class="rows">
        <!-- /# column -->
    <form class="form form_posts" id="form_social" name="form_social" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="col-lg-8 pull-left">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Name <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control name" name="name" id="name"  >
                            
                        </div>
                        <span for="name" class="text-danger val_name" style="display: none;">Name is a required field</span>
                    </div>
                    <div class="form-group">
                        <label for="icon_class">Link</label>
                        <div class="input-group">
                            <input type="text" class="form-control link"  name="link" id="link" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea id="summernote" class="summernote form-control description" name="description"   placeholder="Enter text ..." ><p id="des"></p></textarea>
                        <!-- <input type="hidden" value> -->
                    </div>
                    <div class="form-group">
                        <label for="image"> Image</label>
                        <div class="input-group">
                            <input type="file" class="form-control images" name="images" id="images" placeholder="Upload Image" >
                            
                        </div>
                        <p class="show_image"></p>
                            <input type="hidden" name="images1" value="" id="images1">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 pull-right">
            <div class="card">
                <div class="card-body">
                    <!-- <h4 class="card-title"></h4> -->
                    <div class="card-content">
                            <div class="form-group">
                                <label for="category_id">Select Category</label>
                                <select class="form-control category_id" id="category_id" name="category_id">
                                    <option value="">Select Category</option>
                                    @foreach($categories as $cat)
                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="language">Select Language</label>
                                <select class="form-control language" id="language" name="language">
                                    <option value="">Select Language</option>
                                    @foreach($languages as $lang)
                                        <option value="{{$lang->id}}">{{$lang->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="publish_at">Publish Date</label>                           
                                <input type="text" id="publish_at" class="datepicker publish_at" name="publish_at" placeholder="dd-mm-YYYY">
                            </div>
                            <div class="form-group">
                                <label for="unpublish_at">Unpublish Date</label>                           
                                <input type="text" id="unpublish_at" class="datepicker1 unpublish_at" name="unpublish_at" placeholder="dd-mm-YYYY">
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control status" id="status" name="status">
                                        <option value="1">ACTIVE</option>
                                        <option value="0">INACTIVE</option>
                                </select>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-left">
            <!-- <input type="button" name="submitf" value="Save" id="submitf" class="submit_button floating-button btn btn-info">  -->
            <button type="button" name="submitf" class="submit_button floating-button btn btn-info" value="{{$item_id}}">Submit</button>
            <!-- <button type="reset" class="btn btn-inverse waves-effect waves-light">Cancel</button> -->
        </div>
    </form>
  </div>
  <meta name="_token" content="{{ csrf_token() }}" />
@section('script')
<script>
    var submitting = false;
    function convertSerializedArrayToHash(a) { 
        var r = {};  
        for (var i = 0;i<a.length;i++) {
                if(a[i].name !== 'submitf')
                {
                    r[a[i].name] += a[i].value;
                }
        }
        return r;
    }

 var $form = $('#form_social').eq(0);
// alert($form.serializeArray());
var startItems;
$(document).ready(function() {		
	if(!startItems)
	{
        startItems = convertSerializedArrayToHash($form.serializeArray());
        // alert(startItems);
	}
});
function validate(formdata){
    if(formdata.name != ""){
        $(".val_name").css('display','none');
        return true; 
    }else{
       $(".val_name").removeAttr('style');    
       return false; 
    }
}
$(".name").keyup(function(){
   var name =  $(this).val();
   var formdata = {
        name :name
    }
    validate(formdata);
});
// Function Edit
var item_id = $(".submit_button").val();
if(item_id != 0){
    show_slide(item_id);
}

function show_slide(item_id){
    var url_e = "{{url('slide')}}/"+item_id;
    $.get(url_e, function (data) {
        console.log(data);

        $(".name").val(data.name);
        $(".link").val(data.link);
        // $(".description").val(data.body);
        $("#des").html(data.body);
        $(".description").val(data.body);

        // $(".color").val(data.color);
        // $(".icon_class").val(data.icon_class);
        // $('.menu_type_id option[value="'+data.menu_type_id+'"]').prop('selected', true);
        $('.category_id option[value="'+data.category_id+'"]').prop('selected', true);
        $('.language option[value="'+data.language+'"]').prop('selected', true);
        // var order = Math.round(data.order);
        // $('.order option[value="'+order+'"]').prop('selected', true);
        
        if(data.publish_at != null){
            $('.publish_at').val(day_format_show(data.publish_at));
        }
        if(data.unpublish_at != 1){
            $('.unpublish_at').val(day_format_show(data.unpublish_at));
        }
        $('.status option[value="'+data.status+'"]').prop('selected', true);
        if(data.images != null){
            var show_image = '<br/><img src="'+data.images+'" width="100px" height="100px">';
            $(".show_image").html(show_image);
            $("#images1").val(data.images);
        }
    });
}
$(".submit_button").click(function(){
    var url = "{!! url()->full() !!}";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }

    });
    var item_id = $(this).val();
    // if(item_id == 0){
    //     var url = "";
    // }

    var name = $(".name").val();
    var description = $(".description").val();
    
    var formdata = {
        name :name,
        body: description 
    }
    validate(formdata);

    if(validate(formdata) == true){
       
        var form = document.forms.namedItem("form_social");
        var formDatas = new FormData(form);   
        $.ajax({
            type:'POST', 
            url: url,
            dataType: 'json',
            contentType: false,
            data: formDatas,
            processData: false,
            success: function (result) {
                console.log(result);
                if(item_id == 0){
                    location.reload();
                }else{
                    location.href= "{{url('slide/index')}}";
                }
            },
            error: function (result) {
                console.log('Error:', result);
            }
        });


    }
});


</script>  
@endsection
@endsection