@extends('admin.master.master')
@section('title','Post List')

@section('bottom_header')
    <a href="{{url('video/create?redirect=video&item_id=0')}}" class="btn btn-success pull-right" ><i class="fa fa-plus"></i> Add New </a>
@endsection
@section('content')
  <div class="rows">
        <!-- /# column -->
        <div class="card">
           
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <!-- <th width="3%">#</th> -->
                                <th>Name</th>
                                <th>Link</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($video as $d)
                            @if($d->type == 'video')
                                <tr>
                                    <td>{{$d->name}}</td>
                                    <td>{{$d->link}}</td>
                                    <td>{!! $d->body !!}</td>
                                    <td>
                                        <a href="{{url('video/create?redirect=video&item_id='.$d->id.'')}}" class="btn btn-info btn-rounded m-b-10 m-l-5 edit" ><i class="fa fa-edit"></i> Edit</a>
                                        <form action="{{ url('delete/video/'.$d->id) }}"  id="delete_form" method="POST" style=' display: inline-block;'>
                                            {{ method_field("DELETE") }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-rounded m-b-10 m-l-5 delete_confirm"><i class="fa fa-remove"></i> Delete</button> 
                                        </form> 
                                        
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

  </div> 
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @section('script')
       <!-- scripit init-->
       <!-- <script src="{{ url('js/lib/nestable/jquery.nestable.js') }}"></script> -->
    <!-- scripit init-->
    <!-- <script src="{{ url('js/lib/nestable/nestable.init.js') }}"></script> -->
    <script>
        $(".delete_confirm").click(function(){
            var r = confirm("Are You Sure . Do you want to Delete This Row");
            if (r == true) {
              $("#delete_form").submit();
            } else {
               return false;
            }
        });
        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });
        // $.ajax({
        //     type: 'POST',
        //     url: '{{route("social/json")}}',
        //     dataType: 'json',
        //     contentType: false,
        //     processData: false,
        //     success: function(dat){
        //         console.log(dat);
        //     }
        // });
    </script>
  @endsection
@endsection