@extends('admin.master.master')
@section('title','Menu Type List')

@section('bottom_header')
    <a href="{{url('menus/menu_types/index')}}" class="btn btn-info pull-right" ><i class="fa fa-back"></i> View List </a>
@endsection
@section('content')
  <div class="rows">
        <!-- /# column -->
        <div class="card">
            
            <div class="card-body">
                    <form class="form p-t-20 form_menu_type" id="form_menu_type" name="form_menu_type" method="POST"  >
                    {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <div class="input-group">
                                <input type="text" class="form-control name" name="name" id="name" placeholder="Name" value="{{$data['name']}}">
                                
                            </div>
                            <span for="name" class="text-danger val_name" style="display: none;">Item Name is a required field</span>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail2">Description</label>
                            <textarea id="summernote" class="summernote form-control description" name="description"   placeholder="Enter text ..." >{{$data['description']}}</textarea>
                        </div>
                        
                        <div class="text-left">
                            <input type="button" name="submitf" value="Save" id="submitf" class="submit_button btn btn-info"> 
                            <!-- <button type="button" name="submitf" class="btn btn-success waves-effect waves-light m-r-10 submit_button">Submit</button> -->
                            <button type="reset" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                        </div>
                    </form>
            </div>
        </div>

  </div>
@section('script')
<script>
    var submitting = false;
    function convertSerializedArrayToHash(a) { 
        var r = {};  
        for (var i = 0;i<a.length;i++) {
                if(a[i].name !== 'submitf')
                {
                    r[a[i].name] += a[i].value;
                }
        }
        return r;
    }

var $form = $('#form_menu_type').eq(0);
// alert($form.serializeArray());
var startItems;
$(document).ready(function() {		
	if(!startItems)
	{
        startItems = convertSerializedArrayToHash($form.serializeArray());
        // alert(startItems);
	}
});
function validate(formdata){
    if(formdata.name != ""){
        $(".val_name").css('display','none');
        return true; 
    }else{
       $(".val_name").removeAttr('style');    
       return false; 
    }
}
$(".name").keyup(function(){
   var name =  $(this).val();
   var formdata = {
        name :name
    }
    validate(formdata);
});
$(".submit_button").click(function(){
    var name = $(".name").val();
    var description = $(".description").val();
    var formdata = {
        name :name,
        description: description 
    }
    validate(formdata);
    if(validate(formdata) == true){
        $("#form_menu_type").submit();
    }
});


</script> 
@endsection
@endsection