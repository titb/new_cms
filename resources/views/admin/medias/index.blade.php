@extends('admin.master.master')
@section('title','Media Page')
<!-- add to master style -->

@section('bottom_header')
    <a href="{{url('medias/create?redirect=medias&item_id=0')}}" class="btn btn-success pull-right" ><i class="fa fa-plus"></i> Add New Image </a>
@endsection
<style>
p.iamge_titles {
    text-align: center;
    position: absolute;
    top: 50%;
    left: 14%;
    color: #fff;
    font-weight: 700;
    text-transform: capitalize;
}
</style>
@section('content')
  <div class="rows">
        <div class="col-12">
            <div class="card">
                
                    <div class="rows">

                    <?php $url_np =  url('');
                           $url_np = str_replace('/public','', $url_np);
                    ?>
                        @foreach($fold as $s)
                            <?php 
                                $fo1 = str_replace($dire.'/','',$s);
                              

                            ?>
                            <div class="col-lg-3 col-md-6 m-b-20" style="display: inline-block;">
                                <a href="{{url('medias/'.$fo1.'')}}" >
                                    <img src="{{url('images/folder_blue.png')}}" class="img-responsive radius">
                                    <p class="iamge_titles"> {{$fo1}} </p>
                                </a>
                            </div>
                        @endforeach  
                        @foreach($files  as $k => $f)
                            <?php 
                                $fo1 = str_replace($dire.'/','',$f);
                                $path = $url_np."/storage/app";
                                $url =$path.'/'.$f;
                            ?>
                            <div class="col-lg-3 col-md-6 m-b-20" style="display: inline-block;">
                                <a href="{{$url}}" target="_blank">
                                    <img src="{{$url}}" class="img-responsive radius">
                                    <p class="image_titles"> {{$fo1}} </p>
                                </a>
                            </div>
                        @endforeach
                    </div> 
                </div>
      

        <div>    
  </div>




<!-- add to master script -->
    @section('script') 
          <script src="{{ url('js/lib/dropzone/dropzone.js')}}"></script>
    @endsection    
@endsection