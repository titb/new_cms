@extends('admin.master.master')
@section('title','Media Page')
<!-- add to master style -->
@section('bottom_header')

    <link href="{{ url('css/lib/dropzone/dropzone.css')}} " rel="stylesheet">

@endsection

@section('content')
  <div class="rows">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Media</h4>
                    <h6 class="card-subtitle">For multiple file upload put class <code>.dropzone</code> to form.</h6>
                    <form action="#" class="dropzone">
                        <div class="fallback">
                            <input name="file" type="file" multiple />
                        </div>
                    </form>
                </div>
            </div>

        <div>    
  </div>




<!-- add to master script -->
    @section('script') 
          <script src="{{ url('js/lib/dropzone/dropzone.js')}}"></script>
    @endsection    
@endsection