@extends('admin.master.master')
@section('title','Media Page')
<!-- add to master style -->
@section('bottom_header')

    <link href="{{ url('css/lib/dropzone/dropzone.css')}} " rel="stylesheet">

@endsection
<style>
p.iamge_titles {
    text-align: center;
    position: absolute;
    top: 50%;
    left: 20%;
    color: #fff;
    font-weight: 700;
    text-transform: capitalize;
}
</style>
@section('content')
  <div class="rows">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="rows">
                    <?php $url_np =  url('');
                           $url_np = str_replace('/public','', $url_np);
                    ?>
                        @foreach($fold as $s)
                            <?php 
                                $fo1 = str_replace($dire.'/','',$s);
                              

                            ?>
                            <div class="col-lg-3 col-md-6 m-b-20" style="display: inline-block;">
                                <a href="{{url('medias/'.$fo1.'')}}" >
                                    <img src="{{url('images/folder_blue.png')}}" class="img-responsive radius">
                                    <p class="iamge_titles"> {{$fo1}} </p>
                                </a>
                            </div>
                        @endforeach  
                        @foreach($files  as $k => $f)
                            <?php 
                                $fo1 = str_replace($dire.'/','',$f);
                                $path = $url_np."/storage/app";
                                $url =$path.'/'.$f;
                            ?>
                            <div class="col-lg-3 col-md-6 m-b-20" style="display: inline-block;">
                                <a href="{{$url}}" target="_blank">
                                    <img src="{{$url}}" class="img-responsive radius">
                                    <p class="image_titles"> {{$fo1}} </p>
                                </a>
                            </div>
                        @endforeach
                    </div> 
                </div>
            </div>

        <div>    
  </div>




<!-- add to master script -->
    @section('script') 
          <script src="{{ url('js/lib/dropzone/dropzone.js')}}"></script>
    @endsection    
@endsection