@extends('admin.master.master')
@section('bottom_header')
    <a href="{{ url('permission/create') }}" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add New </a>
@endsection
@section('content')

<style>
    .text-center {
        text-align: center;
    }
    .text-fl-left {
        float: left;
        margin-top: 10px;
    }
    .btn-delete:hover {
        color: #ff346a;
        background-color: #f2f2f2;
        border-color: #ff346a;
    }
</style>

<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/> 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script> -->
 
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <div id="example23_wrapper" class="dataTables_wrapper">
                        <div id="example23_filter" class="dataTables_filter">
                            <label>Search:<input type="search" class="" placeholder="" aria-controls="example23"></label>
                        </div>
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example23_info" style="width: 100%;">
                            <thead>
                                <tr role="row">
                                    <th class="text-center" tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending"
                                        style="width: 10%;">#</th>
                                    <th class="text-center" tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" 
                                        style="width: 200.4px;">Name</th>
                                    <th class="text-center" tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" 
                                        style="width: 164.4px;">Display Name</th>
                                    <th class="text-center" tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending"
                                        style="width: 10%;">Method</th>
                                    <th class="text-center" tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" 
                                        style="width: 164.4px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $key)
                                <tr role="row" class="odd">
                                    <td class="sorting_1 text-center">{{ $key->id }}</td>
                                    <td class="text-center">{{ $key->name }}</td>
                                    <td class="text-center">{{ $key->display_name }}</td>
                                    <td class="text-center">{{ $key->methods->display_name }}</td>
                                    <td class="text-center">
                                        <a href="{{url('permission/'.$key->id.'/edit')}}" class="btn btn-dark btn-outline m-b-10 m-l-5"><i class="fa fa-edit "></i> Edit</a>
                                        <form action = "{{url('permission/'.$key->id.'/delete')}}" method="post" style="display:inline-block;" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn-delete btn btn-dark btn-outline m-b-10 m-l-5"><i class="fa fa-ban"> Delete</i></button>
                                        </form> 
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="dataTables_info" id="example23_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                        <div class="dataTables_paginate paging_simple_numbers" id="example23_paginate"><a class="paginate_button previous" aria-controls="example23" data-dt-idx="0" tabindex="0" id="example23_previous">Previous</a><span><a class="paginate_button " aria-controls="example23" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="example23" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="example23" data-dt-idx="3" tabindex="0">3</a><a class="paginate_button current" aria-controls="example23" data-dt-idx="4" tabindex="0">4</a><a class="paginate_button " aria-controls="example23" data-dt-idx="5" tabindex="0">5</a><a class="paginate_button " aria-controls="example23" data-dt-idx="6" tabindex="0">6</a></span><a class="paginate_button next" aria-controls="example23" data-dt-idx="7" tabindex="0" id="example23_next">Next</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    // $(document).ready( function () {
    //     $('#example23').DataTable();
    // } );
</script>
<!-- <script src="js/lib/datatables/datatables.min.js"></script>
<script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="js/lib/datatables/datatables-init.js"></script> -->


@endsection
