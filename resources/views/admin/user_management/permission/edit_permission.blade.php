@extends('admin.master.master')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ url('permission/'.$data->id.'/edit') }}" method="post">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <p class="text-muted m-b-15 f-s-12">Name</p>
                            <input type="text" class="form-control input-flat" name="name" value="{{ $data->name }}">
                        </div>
                        <div class="form-group">
                            <p class="text-muted m-b-15 f-s-12">Display Name</p>
                            <input type="text" class="form-control input-flat" name="display_name" value="{{ $data->display_name }}">
                        </div>
                        <div class="form-group">
                            <p class="text-muted m-b-15 f-s-12">Method</p>
                            <select class="form-control" name="method">
                                @foreach($method as $val)
                                <option value="{{ $val->id }}"<?php if($val->id == $data->method_id){echo "selected";} ?>>{{ $val->display_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-success btn-flat m-b-10 m-l-5">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection