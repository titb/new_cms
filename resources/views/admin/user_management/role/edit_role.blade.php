@extends('admin.master.master')
@section('content')

<style>
    .checkbox {
        display: block;
        min-height: 20px;
        padding-left: 20px;
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .col-left {
        float: left;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ url('role/'.$data->id.'/edit') }}" method="post">
                    {{ csrf_field() }}
                    <!-- <div class="col-sm-8"> -->
                        <div class="form-group">
                            <p class="text-muted m-b-15 f-s-12">Name</p>
                            <input type="text" class="form-control input-flat" name="name" value="{{ $data->name }}">
                        </div>
                        <div class="form-group">
                            <p class="text-muted m-b-15 f-s-12">Display Name</p>
                            <input type="text" class="form-control input-flat" name="display_name" value="{{ $data->display_name }}">
                        </div>
                        
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="selectall" class="ace" />
                                <span class="lbl">  Select All </span>
                            </label>
                        </div>
                        @foreach($groups as $key => $grouper)
                            <div class="control-group">
                                <label class="col-sm-12 control-label bolder blue" style="color: blue;">
                                    @if($grouper->first()->method_id)
                                        {{$grouper->first()->methods->display_name}}
                                    @endif
                                </label>
                                @foreach($grouper as $permiss)
                                    <div class="col-sm-3 col-left">
                                        <div class="checkbox">
                                            <label>
                                                <input name="permission_id[]" type="checkbox" class="ace permission_id" value="{{ $permiss->id }}" <?php foreach($data->permissions as $val){if($permiss->id == $val->id){echo "checked";}} ?> />
                                                <span class="lbl">{{ $permiss->display_name}}</span>
                                            </label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                       
                    </div>
                    <div style="clear: both;">
                        <button type="submit" class="btn btn-success btn-flat m-b-10 m-l-5">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="http://localhost/shareholder_v1/public/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#selectall').click(function(event) {
			if(this.checked) {
				$('.permission_id').each(function(){//loop through each checkbox
					this.checked = true; //select all checkboxes with class "checkbox1"
				});
			}else{
				$('.permission_id').each(function(){ //loop through each checkbox
					this.checked = false; //deselect all checkboxes with class "checkbox1" 
				});
			}
		});
	});
</script>

@endsection