@extends('admin.master.master')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ url('method/create') }}" method="post">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <p class="text-muted m-b-15 f-s-12">Name</p>
                            <input type="text" class="form-control input-flat" name="name">
                        </div>
                        <div class="form-group">
                            <p class="text-muted m-b-15 f-s-12">Display Name</p>
                            <input type="text" class="form-control input-flat" name="display_name">
                        </div>
                        <button type="submit" class="btn btn-success btn-flat m-b-10 m-l-5">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection