@extends('admin.master.master')
@section('title','Edit User')
@section('bottom_header')
    <a href="{{ url('change/'.$data->id.'/password') }}" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Change Password </a>
@endsection
@section('content')

<div class="col-md-12">
    <form action="{{ url('user/'.$data->id.'/edit') }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}

        <div class="col-md-8">
            <div class="form-group">
                <label>First Name</label>
                <input class="form-control input-flat" name="first_name" value="{{ $data->first_name }}">
            </div>
            <div class="form-group">
                <label>Last Name</label>
                <input class="form-control input-flat" name="last_name" value="{{ $data->last_name }}">
            </div>
            <div class="form-group">
                <label>User Name</label>
                <input class="form-control input-flat" name="username" value="{{ $data->username }}">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input class="form-control input-flat" name="email" value="{{ $data->email }}" disabled>
            </div>
            <div class="form-group">
                <label>Phone</label>
                <input class="form-control input-flat" name="phone" value="{{ $data->phone }}">
            </div>
            <div class="form-group">
                <label> Role</label>
                <select class="form-control input-flat" id="role" name="role">
                    <option value="">Select Role</option>
                    @foreach($roles as $val)
                        <option value="{{ $val->id }}" <?php if($data->user_id){if($val->id == $data->role->first()->id){echo "selected";}} ?>>{{ $val->display_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Stuatus</label>
                <select class="form-control" name="status">
                    <option value="1" <?php if($data->status == 1){echo "selected";} ?>>Active</option>
                    <option value="0" <?php if($data->status == 0){echo "selected";} ?>>Not Active</option>
                </select>
            <!-- </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control input-flat" name="password" value="{{ $data->password }}">
            </div>
            <div class="form-group">
                <label>Comfirm Password</label>
                <input type="password" class="form-control input-flat" name="comfirm_password" value="{{ $data->password }}">
            </div> -->
            <div class="form-group">
                <!-- <label>Photo</label> -->
                <img src="{{url('images/'.$data->image)}}" class="img-file" width="100px" alt="">
                <input type="file" name="image" class="img-input form-control input-flat">
            </div>
            <button type="submit" class="btn btn-success btn-flat m-b-10 m-l-5">Submit </button>

        </div>
    </form>
</div>

@endsection