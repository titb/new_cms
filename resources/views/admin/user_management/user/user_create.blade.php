@extends('admin.master.master')
@section('title','Create User')
@section('content')

<div class="col-md-12">
    <form action="{{ url('user/create') }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}

        <div class="col-md-8">
            <div class="form-group">
                <label>First Name</label>
                <input class="form-control input-flat" name="first_name">
            </div>
            <div class="form-group">
                <label>Last Name</label>
                <input class="form-control input-flat" name="last_name">
            </div>
            <div class="form-group">
                <label>User Name</label>
                <input class="form-control input-flat" name="username">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input class="form-control input-flat" name="email">
            </div>
            <div class="form-group">
                <label>Phone</label>
                <input class="form-control input-flat" name="phone">
            </div>
            <div class="form-group">
                <label> Role</label>
                <select class="form-control input-flat" id="role" name="role">
                    <option value="">Select Role</option>
                    @foreach($data as $val)
                        <option value="{{ $val->id }}">{{ $val->display_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Stuatus</label>
                <select class="form-control" name="status">
                    <option value="1">Active</option>
                    <option value="0">Not Active</option>
                </select>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control input-flat" name="password">
            </div>
            <div class="form-group">
                <label>Comfirm Password</label>
                <input type="password" class="form-control input-flat" name="comfirm_password">
            </div>
            <div class="form-group">
                <label>Photo</label>
                <input type="file" name="image" class="form-control input-flat">
            </div>
            
            <button type="submit" class="btn btn-success btn-flat m-b-10 m-l-5">Submit </button>

        </div>
    </form>
</div>

@endsection