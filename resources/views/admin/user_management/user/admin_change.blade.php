@extends('admin.master.master')
@section('title', 'Change Password')
@section('content')
@include('error.error')
<div class="row">

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ url('change/'.Auth::user()->id) }}" method="post">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <p class="text-muted m-b-15 f-s-12">Old Password</p>
                            <input type="password" class="form-control input-flat" name="old_password">
                        </div>
                        <div class="form-group">
                            <p class="text-muted m-b-15 f-s-12">New Password</p>
                            <input type="password" class="form-control input-flat" name="new_password">
                        </div>
                        <div class="form-group">
                            <p class="text-muted m-b-15 f-s-12">Confirm New Password</p>
                            <input type="password" class="form-control input-flat" name="confirm_password">
                        </div>
                        <button type="submit" class="btn btn-success btn-flat m-b-10 m-l-5">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection