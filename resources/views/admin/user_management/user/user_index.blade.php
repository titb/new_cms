@extends('admin.master.master')
@section('title','User List')
@section('bottom_header')
    <a href="{{ url('user/create') }}" class="space btn btn-success pull-right"><i class="fa fa-plus"></i> Add New </a>
@endsection
@section('content')

<style>
    .text-fl-left {
        float: left;
        margin-top: 10px;
    }
    .btn-delete:hover {
        color: #ff346a;
        background-color: #f2f2f2;
        border-color: #ff346a;
    }
    .space{ margin-right: 5px; }
</style>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <div id="example23_wrapper" class="dataTables_wrapper">
                        <div id="example23_filter" class="dataTables_filter">
                            <label>Search:<input type="search" class="" placeholder="" aria-controls="example23"></label>
                        </div>
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example23_info" style="width: 100%;">
                            <thead>
                                <tr role="row">
                                    <th tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending"
                                        style="width: 3%;">#</th>
                                    <th tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending"
                                        style="width: 15%">Name</th>
                                    <th tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" 
                                        style="width: 15%;">User Name</th>
                                    <th tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" 
                                        style="width: 20%;">Email</th>
                                    <th tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" 
                                        style="width: 10%;">Phone</th>
                                    <th tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" 
                                        style="width: 10%;">Status</th>
                                    <th tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" 
                                        style="width: 10%;">Role</th>
                                    <th tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" 
                                        style="width: 15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $key)
                                <tr role="row" class="odd">
                                    <td>{{ $key->id }}</td>
                                    <td>{{ $key->full_name }}</td>
                                    <td>{{ $key->username }}</td>
                                    <td>{{ $key->email }}</td>
                                    <td>{{ $key->phone }}</td>
                                    <td>
                                        @if($key->status == 1)
                                            Active
                                        @else
                                            Not Active
                                        @endif
                                    </td>
                                    <td>
                                        @foreach($key->roles as $val)
                                            {{ $val->display_name }}
                                        @endforeach
                                    </td>
                                    <td>
                                        <a href="{{url('user/'.$key->id.'/edit')}}" class="btn btn-dark btn-outline m-b-10 m-l-5"><i class="fa fa-edit "></i> </a>
                                        <a href="{{url('user/'.$key->id.'/show')}}" class="btn btn-dark btn-outline m-b-10 m-l-5"><i class="ace-icon fa fa-eye bigger-120 blue"></i></a>
                                        <form action = "{{url('user/'.$key->id.'/delete')}}" method="post" style="display:inline-block;" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-dark btn-delete btn-outline m-b-10 m-l-5"><i class="ace-icon fa fa-trash-o bigger-120 orange"> </i></button>
                                        </form> 
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!-- <div class="dataTables_info" id="example23_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div> -->
                        <!-- <div class="dataTables_paginate paging_simple_numbers" id="example23_paginate">
                            <a class="paginate_button previous disabled" aria-controls="example23" data-dt-idx="0" tabindex="0" id="example23_previous">Previous</a>
                            <span>
                                <a class="paginate_button current" aria-controls="example23" data-dt-idx="1" tabindex="0">1</a>
                                <a class="paginate_button " aria-controls="example23" data-dt-idx="2" tabindex="0">2</a>
                                <a class="paginate_button " aria-controls="example23" data-dt-idx="3" tabindex="0">3</a>
                                <a class="paginate_button " aria-controls="example23" data-dt-idx="4" tabindex="0">4</a>
                                <a class="paginate_button " aria-controls="example23" data-dt-idx="5" tabindex="0">5</a>
                                <a class="paginate_button " aria-controls="example23" data-dt-idx="6" tabindex="0">6</a>
                            </span>
                            <a  class="paginate_button next" aria-controls="example23" data-dt-idx="7" tabindex="0" id="example23_next">Next</a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
