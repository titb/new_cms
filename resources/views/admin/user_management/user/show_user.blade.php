@extends('admin.master.master')
@section('title','Show User')
@section('content')

<style>
    .label-success, .label.label-success, .badge.badge-success, .badge-success {
        background-color: #82af6f;
    }
    .hr.dotted, .hr-dotted {
        border-style: dotted;
        border-color: #278eff;
    }
    .hr {
        display: block;
        height: 0;
        overflow: hidden;
        /* font-size: 0; */
        border-width: 1px 0 0 0;
        border-top: 1px solid #E3E3E3;
        margin: 12px 0;
        border-top-color: rgba(0, 0, 0, .11);
    }
</style>

<div class="row">
    <div class="col-xs-12 col-sm-3 center">
            @if($user->image)
                <img class="editable img-responsive" alt="" id="avatar2" src="{{ url('images/'.$user->image) }}" width="180px" height="200px">
            @endif
        <div class="space space-4"></div>
    </div><!-- /.col -->

    <div class="col-xs-12 col-sm-9">
        
        <div class="profile-user-info">
            <div class="profile-info-row">
                <div class="profile-info-name"> Name :</div>

                <div class="profile-info-value">
                    <span>{{$user->full_name}}</span>
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Phone :</div>

                <div class="profile-info-value">
                    <span>{{$user->phone}}</span>
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Email :</div>

                <div class="profile-info-value">
                    <span>{{ $user->email }}</span>
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Username :</div>

                <div class="profile-info-value">
                    
                    <span>{{ $user->username }}</span>
                    
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Status :</div>

                <div class="profile-info-value">
                    <span class="label label-sm label-success">
                        @if($user->status == 1)
                            Active
                        @else
                            Not Active
                        @endif
                    </span>
                </div>
            </div>
        </div>

        <div class="hr hr-8 dotted"></div>

    </div><!-- /.col -->
</div>

@endsection