@extends('admin.master.master')
@section('title','Category List')

@section('bottom_header')

    <a href="{{url('categories/create?redirect=categories&item_id=0')}}" class="btn btn-success pull-right" ><i class="fa fa-plus"></i> Add New </a>
@endsection
@section('content')
  <div class="rows">
        <!-- /# column -->
        <div class="card">
           
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th><input id="setecte_all"  class="setecte_all" type="checkbox">  ID</th>
                                <th>Name</th>
                                <th>Show Title</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $d)
                            <tr>
                                <th scope="row"><input id="setecte_id"  class="setecte_id" type="checkbox" value="{{$d->id}}"> {{$d->id}}</th>
                                <td>{{$d->name}}</td>
                                <td>
                                    @if($d->is_show_title == 1)
                                        Yes
                                    @else
                                        No
                                    @endif
                                </td>
                                <td>
                                    @if($d->status == 1)
                                        ACTIVE
                                    @else
                                        INACTIVE
                                    @endif
                                </td>
                                <td>
                                        <a href="{{url('categories/create?redirect=categories&item_id='.$d->id.'')}}" class="btn btn-info btn-rounded m-b-10 m-l-5 edit" ><i class="fa fa-edit"></i> Edit</a>
                                     <form action="{{route('categories.deleted',['id'=>$d->id])}}"  id="delete_form" method="POST" style=' display: inline-block;'>
                                        {{ method_field("DELETE") }}
                                        {{ csrf_field() }}

                                     </form> 
                                     <button type="button" class="btn btn-danger btn-rounded m-b-10 m-l-5 delete_confirm"><i class="fa fa-remove"></i> Delete</button>  
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

  </div> 

  @section('script')
       <!-- scripit init-->
       <script src="{{ url('js/lib/nestable/jquery.nestable.js') }}"></script>
    <!-- scripit init-->
    <script src="{{ url('js/lib/nestable/nestable.init.js') }}"></script>
    <script>
        $(".delete_confirm").click(function(){
            var r = confirm("Are You Sure . Do you want to Delete This Row");
            if (r == true) {
              $("#delete_form").submit();
            } else {
               return false;
            }
        });
    </script>
  @endsection
@endsection