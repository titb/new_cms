 <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="site-section-heading text-center mb-5 w-border col-md-6 mx-auto">
            <h2 class="mb-5">{{$module_post->name}}</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet, fugit nam obcaecati fuga itaque deserunt
              officia, error reiciendis ab quod?</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="100">
            <a href="#" class="unit-9">
              <div class="image" style="background-image: url('images/img_1.jpg');"></div>
              <div class="unit-9-content">
                <h2>Nashville</h2>
                <span>$130/night</span>
                <!-- <span>with Wendy Matos</span> -->
              </div>
            </a>
          </div>

          <div class="col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="200">
            <a href="#" class="unit-9">
              <div class="image" style="background-image: url('images/img_2.jpg');"></div>
              <div class="unit-9-content">
                <h2>Baltimore</h2>
                <span>$230/night</span>
              </div>
            </a>
          </div>
          <div class="col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="300">
            <a href="#" class="unit-9">
              <div class="image" style="background-image: url('images/img_3.jpg');"></div>
              <div class="unit-9-content">
                <h2>Austin</h2>
                <span>$130/night</span>
              </div>
            </a>
          </div>
          <div class="col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="400">
            <a href="#" class="unit-9">
              <div class="image" style="background-image: url('images/img_4.jpg');"></div>
              <div class="unit-9-content">
                <h2>Atlanta</h2>
                <span>$150/night</span>
              </div>
            </a>
          </div>

          <div class="col-md-12 text-center mt-5" data-aos="fade-up">
            <a href="#" class="btn btn-primary">Browse All Apartments</a>
          </div>
        </div>
      </div>
    </div>