<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public function Menus()
    {
        return $this->belongsToMany('App\Menu','menu_module','module_id','menu_id');
    }
    public function posts(){
        return $this->belongsToMany('App\Post','post_module','module_id','module_type_id');
    }
    public function categorys(){
        return $this->belongsToMany('App\Category','post_module','module_id','module_type_id');
    }
}
