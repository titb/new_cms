<?php

namespace App\Http\Middleware;

use Closure;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user() === null){
            
            return back()->with('errorskey','You dont have Permission');
    
            }
    
            $actions =$request->route()->getAction();
    
            $permissions = isset($actions['permissions']) ? $actions['permissions'] : null;
    
    
    
            if($request->user()->roles->first()->hasAnyPermission($permissions) || !$permissions){
                
                return $next($request);
                
            }
    
            return back()->with('errorskey','You dont have Permission');
    
    }
}
