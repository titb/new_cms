<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Menu;

class TemplateController extends Controller
{
    public function home_page(){
        $Menus = Menu::get();
        return view('templates.index',compact('Menus'));
    }
    
    public function scan_dir(){
        $dir = resource_path('views');
        if (is_dir($dir)){
            if ($dh = opendir($dir)){
                while (($file = readdir($dh)) !== false){
                echo $file . "<br>";
                }
                closedir($dh);
            }
        }
    }
    public function load_xml(){
        $dir = resource_path('views\templates\templateDetails.xml');
        $xmldata = simplexml_load_file($dir) or die("Failed to load");
        // dd($xmldata);
        foreach($xmldata as $k=>$x){
            if($k == 'modules'){
                // foreach($x as $xx){
                //     echo $xx->title.'<br>';
                // }
                $key = $x;
                $this->xml_loop($key);
                // $datas = [
                //     'firstname'=> $x->firstname,
                //     'lastname' => $x->lastname,
                //     'designation' => $x->designation,
                //     'salary' => $x->salary,
                // ];
                // DB::table('emps')->insert($datas);
            }
            if($k == 'positions'){
                // foreach($x->position as $v){
                //     echo $v.'<br>';
                // }
                $key = $x->position;
                $this->xml_loop($key);    
                
            }
            if($k == 'styles'){
                // foreach($x->style as $s){
                //     echo $s.'</br>';
                // }
                $key = $x->style;
                $this->xml_loop($key);    
            }
        }
    }
    public function xml_loop($key){
        foreach($key as $val){
            echo $val.'</br>';
        }
    }
}