<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use App\MenuType;
use App\Menu;
use App\Post;
use App\Category;
use App\Language;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class PostController extends Controller
{
    public function get_posts_all(){
        $data = Post::where('type','=','post')->where('deleted',1)->paginate(15);
        return $data;
    }
    //
    public function index_posts(Request $request){
        $title = "Post List";
        $data = $this->get_posts_all();
        return view('admin.posts.index',compact('data','title'));
    }

    public function  create_posts(Request $request){
        $title = "Create Post";
        $item_id = $request->item_id;
        $lang = Language::where("status",1)->get();
        $menus = Menu::where('deleted',1)->get();
        $categories = Category::where('deleted',1)->get();
        return view('admin.posts.form',compact('title','lang','menus','categories','item_id'));
    }

    public function post_create_posts(Request $request){
       $data = $request->all();
        $slug = $this->getSlug($request->name);
        if($request->publish_at != null){
            $publish_at = date('Y-m-d',strtotime($request->publish_at));
        }else{
            $publish_at = null;
        }
        if($request->unpublish_at != null){
            $unpublish_at = date('Y-m-d',strtotime($request->unpublish_at));
        }else{
            $unpublish_at = null;
        }
        if($request->hasFile('images')){
            $image = $request->file('images');
            $image_url = $this->move_image($image);
        }else{
            $image_url = $request->images1;
        }
        
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }else{
            $user_id = 1;
        }

        if($request->link){
            $link = $request->link;
        }else{
            $link = url($slug);
        }
        
        $data_create = [
            'user_id' => $user_id,
            'parent_id' => 0,
            'category_id' => $request->category_id,
            'name' => $request->name,
            'link' => $link,
            'slug' => $slug,
            'status' => $request->status,
            'language' => $request->language,
            'body' => $request->description,
            'type'=> 'post',
            'images' =>$image_url ,
            'publish_at' => $publish_at,
            'unpublish_at' => $unpublish_at,
            'is_show_title' => 1,
            'deleted' => 1,
        ];
        // Order
        if($request->item_id == 0){
            $post_id = Post::insertGetId($data_create);
            if($request->order != 0){
                $order = $request->order + 0.01;  
           }else{
                $order =  $post_id;
           }
            Post::where('id','=',$post_id)->update(['order'=> $order, 'created_at'=>date('Y-m-d h:m:s')]); 
        }else{
            
            $id = $request->item_id;
            $me = Post::find($id);

           if($request->order != $me->order){
                $order = $request->order + 0.01;  
           }else{
                $order =  $request->order;
           }
           $data_update = array_add($data_create,'order', $order);
           Post::where('id','=',$id)->update($data_update); 
        }
        return response()->json($data_create);
    }
    
    public function get_posts_json($id){
        $data = Post::find($id);
        return response()->json($data);
    }
    public function getSlug($slug){
        $slug_con = str_slug($slug, '-');
        if($slug_con != ""){
            return $slug_con;
        }
        $slug = preg_replace('/[^\\pL\d_]+/u', '-',$slug);
        return $slug;
    }
    
    
    public function move_image($image){
        $url_np =  url('');
        $url_np = str_replace('/public','', $url_np);
        $im = $image;
        $fileName = $im->getClientOriginalName();
        $storage = date('M-Y');
        $destinationPath = storage_path('app/public/images/'.$storage) ;
            $up = $im->move($destinationPath,$fileName);
            $image_name = $destinationPath."/".$fileName;
            $path = $url_np."/storage/app/public/images/".$storage;
            $url =$path.'/'.$fileName;
            return $url;
    }

    public function delete_posts($id) {
        Post::destroy($id);
        return redirect()->back();
        // echo "$id";
    }
}
