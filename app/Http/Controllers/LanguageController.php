<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use App\Language;
use DB;

class LanguageController extends Controller
{
    public function index_language(){
        $title = "Language";
        $language = Language::get();
        return view('admin.language.index',compact('title','language'));
    }
    public function delete_lang($id){
        $del_data = Language::find($id);
        $del_data->delete();
        return redirect()->back();
    }
    public function create_lang(Request $request){
        $title = "Language";
        $item_id = $request->item_id;
        return view('admin.language.form',compact('title','item_id'));
    }
    public function post_create_lang(Request $request){
        $lang_data = [
                    'name' => $request->name,
                    'zipcode' => $request->zip_code,
                    'code' => $request->code,
                    'status' => 1,
                ];
        if($request->item_id == 0){
            $lang_id = Language::insertGetId($lang_data);
            Language::where('id','=',$lang_id)->update(['created_at' => date('Y-m-d h:m:s')]);
        }
        else{
            $id = $request->item_id;
            Language::where('id','=',$id)->update($lang_data, ['updated_at'=>date('Y-m-d')]);
        }
        return response()->json($lang_data);
    }
    public function lang_json($id){
        $lang = Language::find($id);
        return response()->json($lang);
    }
}
