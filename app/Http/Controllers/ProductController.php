<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use App\Post;
use App\Category;
use App\ProductCategory;
use App\ProductBrand;
use App\Language;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class ProductController extends Controller
{

   public function index_brand (){
        $title = "Brands";
        $brand = ProductBrand::where('deleted','=',0)->get();
        return view('admin.product.brand.index',compact('title','brand'));
    }
    public function create_brand (request $request){
        $title = "Brands";
        $item_id = $request->item_id;
        $categories = ProductCategory::where('deleted','=',0)->get();
        $languages = Language::where('status','=',1)->get();
        return view('admin.product.brand.form',compact('title','categories','languages','item_id'));
    }
    public function post_brand (request $request){
        if($request->hasFile('images')){
            $image = $request->file('images');
            $image_url = $this->move_image($image);
        }else{
            $image_url = $request->images1;
        }
        $data_brand = [
            'name'=>$request->name,
            'code'=>$request->code,
            'description'=>$request->description,
            'images'=>$image_url, 
            'category'=>$request->category_id,
            'language'=>$request->language,
        ];
        $item_id = $request->item_id;
        if($item_id == 0){
            $brand_id = ProductBrand::insertGetId($data_brand);
            ProductBrand::where('id','=',$brand_id)->update(['created_at'=>date('y-m-d h:m:s')]);
        }else{
            $brand_id = ProductBrand::where('id',$item_id)->update($data_brand);
        }
        return response()->json($brand_id);
    }
    public function move_image($image){
        $url_np =  url('');
        $url_np = str_replace('/public','', $url_np);
        $im = $image;
        $fileName = $im->getClientOriginalName();
        $storage = date('M-Y');
        $destinationPath = storage_path('app/public/images/'.$storage) ;
            $up = $im->move($destinationPath,$fileName);
            $image_name = $destinationPath."/".$fileName;
            $path = $url_np."/storage/app/public/images/".$storage;
            $url =$path.'/'.$fileName;
            return $url;
    }
    Public function json_edit_brand(request $request){
        $item_id = $request->item_id;
        $brand_data = ProductBrand::find($item_id);
        return response()->json($brand_data);
    }
    public function delete_brand($id){
        $del_brand = ProductBrand::find($id);
        $del_brand->delete();
        return redirect()->back();
    }

}