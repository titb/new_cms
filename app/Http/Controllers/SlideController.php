<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\Language;

class SlideController extends Controller
{
    public function index_slide(){
        $title = "Video";
        $slide = Post::where('type','=','slide')->get();
        return view('admin.slide.index',compact('title','slide'));
    }
    public function create_slide(Request $request){
        $title = "Create Slide";
        $item_id = $request->item_id;
        $categories = Category::get();
        $languages = Language::get();
        return view('admin.slide.form',compact('title','item_id','categories','languages'));
    }
    public function post_create_slide(Request $request){
        if($request->hasFile('images')){
            $image = $request->file('images');
            $image_url = $this->move_image($image);
        }else{
            $image_url = $request->images1;
        }

        $data_slide = [
            // Add user id
            'name'=>$request->name,
            'link'=>$request->link,
            'body'=>$request->description,
            'images'=>$image_url,
            'status'=>$request->status,
            'category_id'=>$request->category_id,
            'language'=>$request->language,
            'publish_at'=>date('Y-m-d', strtotime($request->publish_at)),
            'unpublish_at'=>date('Y-m-d', strtotime($request->unpublish_at)),
            'type'=>'slide',
        ];
        if($request->item_id == 0){
            $post_id = Post::insertGetId($data_slide);
            // Post::where('id','=',$social_id)->update(['created_at'=>date('Y-m-d h:m:s')]);
            Post::where('id','=',$post_id)->update(['created_at'=>date('Y-m-d h:m:s')]); 
        }else{
            
            $id = $request->item_id; //Request from url
            // $me = Post::find($id);
        //    $data_update = array_add($data_social);
            Post::where('id','=',$id)->update($data_slide,['updated_at'=>date('Y-m-d h:m:s')]); 
        }
        return response()->json($data_slide);
        // print_r($data);
    }
    public function json_slide($id){
        $slide = Post::find($id);
        return response()->json($slide);
    }
    public function move_image($image){
        $url_np =  url('');
        $url_np = str_replace('/public','', $url_np);
        $im = $image;
        $fileName = $im->getClientOriginalName();
        $storage = date('M-Y');
        $destinationPath = storage_path('app/public/images/'.$storage) ;
            $up = $im->move($destinationPath,$fileName);
            $image_name = $destinationPath."/".$fileName;
            $path = $url_np."/storage/app/public/images/".$storage;
            $url =$path.'/'.$fileName;
            return $url;
    }
    public function deleted_slide($id){
        $slide = Post::find($id);
        $slide->delete();
        return Redirect()->back();
    }
}
