<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use App\MenuType;
use App\Menu;
use App\Language;
use Auth;
use App\Category;
use Illuminate\Http\File;
class CategoryController extends Controller
{
    public function get_category_all(){
        $data = Category::where('deleted',1)->paginate(15);
        return $data;
    }
    //
    public function index_category(Request $request){
        $title = "Category List";
        $data = $this->get_category_all();
        return view('admin.categories.index',compact('data','title'));
    }
    public function  create_categories(Request $request){
        $title = "Create Category";
        $item_id = $request->item_id;
        $lang = Language::where("status",1)->get();
        // $menu_types = MenuType::all();
        $cat = Category::where('deleted',1)->get();
        return view('admin.categories.form',compact('title','lang','menu_types','cat','item_id'));
    }
    public function post_create_categories(Request $request){
        $data = $request->all();
         $slug = $this->getSlug($request->name);
         if($request->publish_at != null){
             $publish_at = date('Y-m-d',strtotime($request->publish_at));
         }else{
             $publish_at = null;
         }
         if($request->unpublish_at != null){
             $unpublish_at = date('Y-m-d',strtotime($request->unpublish_at));
         }else{
             $unpublish_at = null;
         }
         if($request->hasFile('images')){
             $image = $request->file('images');
             $image_url = $this->move_image($image);
         }else{
             $image_url = $request->images1;
         }
         
         if(Auth::check()){
             $user_id = Auth::user()->id;
         }else{
             $user_id = 1;
         }
         
         $data_create = [
             'user_id' => $user_id,
            //  'menu_type_id' => $request->menu_type_id,
             'parent_id' => $request->parent_id,
             'name' => $request->name,
             'link' => $request->link,
             'slug' => $slug,
             'status' => $request->status,
             'language' => $request->language,
            //  'description' => $request->description,
             'images' =>$image_url ,
             'publish_at' => $publish_at,
             'unpublish_at' => $unpublish_at,
            //  'color' => $request->color,
            //  'icon_class' => $request->icon_class,
            //  'target' => $request->target,
             'is_show_title' => 1,
             'deleted' => 1,
            //  'lavel' => 0,
         ];
         if($request->item_id == 0){
             $menu_id = Category::insertGetId($data_create);
             if($request->order != 0){
                 $order = $request->order + 0.01;  
            }else{
                 $order =  $menu_id;
            }
             Category::where('id','=',$menu_id)->update(['order'=> $order, 'created_at'=>date('Y-m-d h:m:s')]); 
         }else{
             
             $id = $request->item_id;
             $me = Category::find($id);
 
            if($request->order != $me->order){
                 $order = $request->order + 0.01;  
            }else{
                 $order =  $request->order;
            }
            $data_update = array_add($data_create,'order', $order);
            Category::where('id','=',$id)->update($data_update); 
         }
         return response()->json($data_create);
     }
     
     public function get_category_json($id){
         $data = Category::find($id);
         return response()->json($data);
     }
     public function getSlug($slug){
         $slug_con = str_slug($slug, '-');
         if($slug_con != ""){
             return $slug_con;
         }
         $slug = preg_replace('/[^\\pL\d_]+/u', '-',$slug);
         return $slug;
     }
     public function delete_category(Request $request,$id){
        $cat = Category::find($id);
        $cat->delete(); 
        return redirect()->back()->with('success','Successfull Deleted Menu');
    }
    public function move_image($image){
        $url_np =  url('');
        $url_np = str_replace('/public','', $url_np);
        $im = $image;
        $fileName = $im->getClientOriginalName();
        $storage = date('M-Y');
        $destinationPath = storage_path('app/public/images/'.$storage) ;
            $up = $im->move($destinationPath,$fileName);
            $image_name = $destinationPath."/".$fileName;
            $path = $url_np."/storage/app/public/images/".$storage;
            $url =$path.'/'.$fileName;
            return $url;
    }
}