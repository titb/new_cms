<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Role;
use DB;
use Input;
use Auth;
use Hash;

class UserController extends Controller {
    public function get_index(){
        $title = "User";
        $data = User::get();
        return view('admin.user_management.user.user_index', compact('data','title'));
    }
    public function get_create_user(){
        $title = "Create";
        $data = Role::get();
        return view('admin.user_management.user.user_create', compact('data','title'));
    }
    public function post_create_user(Request $request)
    {
    	$this->validate($request,[
            // 'email'=>'required|unique:users',
    		'username'=>'required|unique:users',
    		'password'=>'required',
    		'comfirm_password'=>'required|same:password'
    	]);

        if($request->hasFile('image')){
            $images = $request->file('image');
            $destinationPath = "images/";
            $fileName = $images->getClientOriginalName();
            $fileupload = $images->move($destinationPath,$fileName);
        }else{
             $fileName ="";
        }
    	$data = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'full_name' => $request->first_name." ".$request->last_name,
    		'username' => $request->username,
            'image' => $fileName,
    		'email' => $request->email,
            'phone' => $request->phone,
    		'status' => $request->status,
    		'password' => bcrypt($request->password),
    	];

    	$user_id = User::insertGetId($data);
            if($user_id != 0){
                User::where('id','=',$user_id)->update(['usercode'=>'US'.$user_id]);
            }
    		if($user_id != 0 && $request->role){
                $data_role = [
                    'role_id'=> $request->role,
                    'user_id'=> $user_id
                ];
                DB::table('user_roles')->insert($data_role);
            }
    		return Redirect()->to('user/index')->with('success','Successfull Create');
    }
      public function get_edit_user($id)
    {
        $title = "Edit User";
    	$roles = Role::all();
    	$data = User::find($id);
    	return view('admin.user_management.user.user_edit',compact('roles','data','title'));
    }
    public function post_edit_user(Request $request, $id)
    {
        $user = User::find($id);

    	$this->validate($request,[
    		'username'=>'required',
    	]);

        if($request->hasFile('image')){
            $images = $request->file('image');
            $destinationPath = "images/";
            $fileName = $images->getClientOriginalName();
            $fileupload = $images->move($destinationPath,$fileName);
        }else{
             $fileName ="";
        }
    	$data = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'full_name' => $request->first_name." ".$request->last_name,
    		'username' => $request->username,
            'image' => $fileName,
            'phone' => $request->phone,
    		'status' => $request->status,
    	];

        $user_id = User::where('id','=',$id)->update($data);
        
            if($user_id != 0){
                User::where('id','=',$user_id)->update(['usercode'=>'US'.$user_id]);
            }

    		if(count($user->roles) > 0){
    			if($request->role){
                    $data_role =[
                        'role_id'=>$request->role,
                        'user_id'=>$id
                    ];
                    DB::table('user_roles')->where('user_id','=',$id)->update($data_role);
                }
            }else{
                    if($request->has('role')){
                        DB::table('user_roles')->where('user_id',$id)->delete();
                            $data_role =[
                                'role_id'=>$request->role,
                                'user_id'=>$id
                            ];
                            DB::table('user_roles')->insert($data_role);
                    }
            }
    		return Redirect()->to('user/index')->with('success','Successfull Create');
    }
    public function deleted($id){
        User::where('id','=',$id)->delete();
        return Redirect()->to('user/index');
    }
    public function change_pass($id) {
        $title = "Change Password";
        $data = User::find($id);
        return view('admin.user_management.user.change_pass',compact('title','data'));
    }
//Admin Change Password without old password
    public function post_change_pass(Request $request, $id){
        $this->validate($request,[
    		'new_password'=>'required',
    		'confirm_password'=>'required|same:new_password',
    	]);
        $user = User::find($id);

        $password = bcrypt($request->new_password);
        $user->update(['password'=>$password]);
        
        return redirect()->back()->with('success','Successfull Change');
    }
    public function show_user($id) {
        $user = User::find($id);
        $name = $user->full_name;
        $title = $name;
        return view('admin.user_management.user.show_user',compact('user','title'));
    }
    // Change Password with Old Password
    public function admin_change_pass($id){
        $title = "Change Password";
        return view('admin.user_management.user.admin_change', compact('title'));
    }
    public function post_admin_change_pass(Request $request, $id){

        $this->validate($request,[
            'old_password'=>'required',
    		'new_password'=>'required',
    		'confirm_password'=>'required|same:new_password'
    	]);
        $user = Auth::user();
        $id = Auth::user()->id;
        $old_password = $request->old_password;
        $password = bcrypt($request->new_password);

        if (Hash::check($old_password, $user->password)) {
            $user->password = $password;
            try {
                $user->save();
                $flag = TRUE;
            }
            catch(\Exception $e){
                $flag = FALSE;
            }
            if($flag){
                return redirect('login')->with('success',"Password Changed Successfully.");
            }
            else{
                
                return redirect('change/'.$id)->with("error","Unable to process request this time. Try again later");
            }
        }
        else{
            return redirect('change/'.$id)->with("errorskey","Your old password do not match !!");
        }
    }
}