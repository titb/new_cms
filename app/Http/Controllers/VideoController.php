<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\Language;
class VideoController extends Controller
{
    public function index_video(){
        $title = "Video";
        $video = Post::where('type','=','video')->get();
        return view('admin.video.index',compact('title','video'));
    }
    public function create_video(Request $request){
        $title = "Create Video";
        $item_id = $request->item_id;
        $categories = Category::get();
        $languages = Language::get();
        return view('admin.video.form',compact('title','item_id','categories','languages'));
    }
    public function post_create_video(Request $request){
        if($request->hasFile('video')){
            $video = $request->file('video');
            $video_url = $this->move_video($video);
        }else{
            $video_url = $request->video1;
        }

        $data_video = [
            // Add user id
            'name'=>$request->name,
            'link'=>$request->link,
            'body'=>$request->description,
            'images'=>$video_url,
            'status'=>$request->status,
            'category_id'=>$request->category_id,
            'language'=>$request->language,
            'publish_at'=>date('Y-m-d', strtotime($request->publish_at)),
            'unpublish_at'=>date('Y-m-d', strtotime($request->unpublish_at)),
            'type'=>'video',
        ];
        if($request->item_id == 0){
            $post_id = Post::insertGetId($data_video);
            // Post::where('id','=',$social_id)->update(['created_at'=>date('Y-m-d h:m:s')]);
            Post::where('id','=',$post_id)->update(['created_at'=>date('Y-m-d h:m:s')]); 
        }else{
            
            $id = $request->item_id; //Request from url
            // $me = Post::find($id);
        //    $data_update = array_add($data_social);
            Post::where('id','=',$id)->update($data_video,['updated_at'=>date('Y-m-d h:m:s')]); 
        }
        return response()->json($data_video);
        // print_r($data);
    }
    public function json_video($id){
        $video = Post::find($id);
        return response()->json($video);
    }
    public function move_video($video){
        $url_np =  url('');
        $url_np = str_replace('/public','', $url_np);
        $vi = $video;
        $fileName = $vi->getClientOriginalName();
        $storage = date('M-Y');
        $destinationPath = storage_path('app/public/images/'.$storage) ;
        $up = $vi->move($destinationPath,$fileName);
        $video_name = $destinationPath."/".$fileName;
        $path = $url_np."/storage/app/public/images/".$storage;
        $url =$path.'/'.$fileName;
        return $url;
    }
    public function deleted_video($id){
        $video = Post::find($id);
        $video->delete();
        return Redirect()->back();
    }
}
