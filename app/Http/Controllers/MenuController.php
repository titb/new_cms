<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use App\MenuType;
use App\Menu;
use App\Language;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
class MenuController extends Controller
{
    use SerializesModels;
    public $menu_type;
    
    public function get_menu_type_all(){
        $data = MenuType::paginate(15);
        return $data;
    }
    //
    public function index_menu_type(Request $request){
        $title = "Menu Type List";
        $data = $this->get_menu_type_all();
        return view('admin.menu_types.index',compact('data','title'));
    }

    public function  create_menu_type(Request $request){
        $title = "Create Menu Type";
        
        if($request->item_id == 0){
           $data = [
               'name' =>'',
               'description' =>''
           ];
        }else{
           $id = $request->item_id;
           $d = MenuType::find($id);
           $data = [
                'name' => $d->name,
                'description' => $d->description
            ];
        }
        return view('admin.menu_types.form',compact('title','data'));
    }

    public function post_create_menu_type(Request $request){
        // $data = print_r($request->all());
        if($request->item_id == 0){
            $menu = new MenuType;
            $menu->create($request->all());
            return redirect()->back()->with('success','Successfull Create Menu');
       
        }else{
            $id = $request->item_id;
            $menu = MenuType::find($id);
            $menu->update($request->all());
            return redirect('menus/menu_types/index')->with('success','Successfull Update Menu');
        }
       
    }  
    
    public function deleted_menu_version(Request $request,$id){
        $menu = MenuType::find($id);
        $menu->delete(); 
        return redirect()->back()->with('success','Successfull Deleted Menu');
    }

   
    public function get_menus_all(){
        $data = Menu::where('deleted',1)->paginate(15);
        return $data;
    }
    //
    public function index_menus(Request $request){
        $title = "Menu List";
        $data = $this->get_menus_all();
        return view('admin.menus.index',compact('data','title'));
    }

    public function  create_menus(Request $request){
        $title = "Create Menu";
        
        // if($request->item_id == 0){
        //    $data = [
        //        'name' =>'',
        //        'description' =>''
        //    ];
        // }else{
        //    $id = $request->item_id;
        //    $d = MenuType::find($id);
        //    $data = [
        //         'name' => $d->name,
        //         'description' => $d->description
        //     ];
        // }
        $item_id = $request->item_id;
        $lang = Language::where("status",1)->get();
        $menu_types = MenuType::all();
        $menus = Menu::where('deleted',1)->get();
        return view('admin.menus.form',compact('title','lang','menu_types','menus','item_id'));
    }


    public function post_create_menu(Request $request){
       $data = $request->all();
        $slug = $this->getSlug($request->name);
        if($request->publish_at != null){
            $publish_at = date('Y-m-d',strtotime($request->publish_at));
        }else{
            $publish_at = null;
        }
        if($request->unpublish_at != null){
            $unpublish_at = date('Y-m-d',strtotime($request->unpublish_at));
        }else{
            $unpublish_at = null;
        }
        if($request->hasFile('images')){
            $image = $request->file('images');
            $image_url = $this->move_image($image);
        }else{
            $image_url = $request->images1;
        }
        
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }else{
            $user_id = 1;
        }

        if($request->link != null){
            $link = $request->link;
        }else{
            $link = $slug;
        }
        
        $data_create = [
            'user_id' => $user_id,
            'menu_type_id' => $request->menu_type_id,
            'parent_id' => $request->parent_id,
            'name' => $request->name,
            'link' => $link,
            'slug' => $slug,
            'status' => $request->status,
            'language' => $request->language,
            'description' => $request->description,
            'images' =>$image_url ,
            'publish_at' => $publish_at,
            'unpublish_at' => $unpublish_at,
            'color' => $request->color,
            'icon_class' => $request->icon_class,
            'target' => $request->target,
            'is_show_title' => 1,
            'deleted' => 1,
            'lavel' => 0,
        ];
        if($request->item_id == 0){
            $menu_id = Menu::insertGetId($data_create);
            if($request->order != 0){
                $order = $request->order + 0.01;  
           }else{
                $order =  $menu_id;
           }
            Menu::where('id','=',$menu_id)->update(['order'=> $order, 'created_at'=>date('Y-m-d h:m:s')]); 
        }else{
            
            $id = $request->item_id;
            $me = Menu::find($id);

           if($request->order != $me->order){
                $order = $request->order + 0.01;  
           }else{
                $order =  $request->order;
           }
           $data_update = array_add($data_create,'order', $order);
           Menu::where('id','=',$id)->update($data_update); 
        }
        return response()->json($data_create);
    }
    
    public function get_menu_json($id){
        $data = Menu::find($id);
        return response()->json($data);
    }
    public function getSlug($slug){
        $slug_con = str_slug($slug, '-');
        if($slug_con != ""){
            return $slug_con;
        }
        $slug = preg_replace('/[^\\pL\d_]+/u', '-',$slug);
        return $slug;
    }
    
    
    public function move_image($image){
        $url_np =  url('');
        $url_np = str_replace('/public','', $url_np);
        $im = $image;
        $fileName = $im->getClientOriginalName();
        $storage = date('M-Y');
        $destinationPath = storage_path('app/public/images/'.$storage) ;
            $up = $im->move($destinationPath,$fileName);
            $image_name = $destinationPath."/".$fileName;
            $path = $url_np."/storage/app/public/images/".$storage;
            $url =$path.'/'.$fileName;
            return $url;
    }
    public function delete_menu($id){
        $menu = Menu::find($id);
        $menu->delete();
        return Redirect()->back()->with('Success','Delete Successful');
    }
}
