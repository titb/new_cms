<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module;
use Auth;
use DB;

class ModuleController extends Controller
{
    public function module_index()
    {
        $module = Module::all();
        return  view('admin.modules.index', compact('module'));
    }
    //create
    public function module_get_create()
    {
        $module = Module::all();
        $module_edit_id = 0;
        return view('admin.modules.form', compact('module_edit_id', 'module'));
    }
    //post_create
    public function post_create_module(Request $request)
    {
        // dd($request->all());
        // return false;
        // $show_menu_type=0;
        $module = [
            'title'             =>  $request->title,
            'show_menu_type'    =>  $request->show_menu_type,
            'ordering'          =>  $request->ordering,
            'position'          =>  $request->position,
            'module'            =>  $request->module,
            'show_title'        =>  $request->show_title,
            'user_id'           =>  Auth::user()->id,
            'language'          =>  $request->language,

        ];
        // dd($module);
        // dd($request->btn_submit);
        if ($request->btn_submit == 0) {
            $data_module = Module::insertGetId($module);
            if ($data_module != 0) {
                foreach ($request->check as $key => $val) {
                    $menu_module = [
                        'module_id' =>  $data_module,
                        'menu_id'   =>  $request->check[$key],
                    ];
                    // dd($menu_module);
                    DB::table('menu_module')->insert($menu_module);
                }
                if ($request->post_id) {
                    if ($request->module_type == 'post') {
                        $is_category = 0;
                    } else {
                        $is_category = 1;
                    }
                    DB::table('post_module')->insert(['module_id' => $data_module, 'module_type_id' => $request->post_id, 'is_category' => $is_category]);
                }
            }
        } else {
            DB::table('menu_module')->where('module_id', '=', $request->btn_submit)
                ->delete();
            foreach ($request->check as $key => $val) {
                $menu_module = [
                    'module_id' =>  $request->btn_submit,
                    'menu_id'   =>  $request->check[$key],
                ];
                // dd($menu_module);
                DB::table('menu_module')->insert($menu_module);
            }
            $data_module = Module::where('id', '=', $request->btn_submit)->update($module);
        }
        // return redirect()->to('module_get_create');
        return response()->json($module);
    }

    // public function get_posts_json($id){
    //     $data = Module::find($id);
    //     return response()->json($data);
    // }

    //edit
    public function module_get_edit($id)
    {
        $module_edit_id = $id;
        $module = Module::all();
        return view('admin.modules.form', compact('id', 'module_edit_id', 'module'));
    }
    public function module_post_edit($id)
    {
        // $module=Module::find($id);
        $module = Module::with(['Menus'])->where('id', '=', $id)->first();
        return response()->json($module);
    }
    //delete
    public function module_post_delete($id)
    {
        $module = Module::find($id)->delete();
        DB::table('menu_module')->where('module_id', '=', $id)->delete();
        return redirect()->back();
    }
}
