<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    public function login()
    {
      $title = "Backend Login";
      $name_companey = "Welcome";
        return view('admin.login.login');
    }
    public function post_login(Request $request)
    {
        
        $rules = $this->validate($request ,[
            'email' => 'required',
            'password' => 'required'
        ]);
        // 
        if(Auth::attempt(['email'=>$request->email ,'password'=>$request->password , 'status' => 1 ]))
        {
            return redirect('admin')->with('success','Sucessful');
        }
        else
        {

            return redirect('login')->with('errorskey','The user and password information is invalid');

        }

    }
    public function logout()
    {
       Auth::logout();
        return redirect()->to('login');
    }
}