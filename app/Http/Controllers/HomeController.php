<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Menu;

class HomeController extends Controller
{
    public function index() {
        $title = "Daseboard";
        return view('admin.index',compact('title'));
    }

    public function template_index($link){
        $title = "Template";
        $Menus = Menu::get();
        $Menu_Module = Menu::where('link',$link)->first();
        return view('templates.index',compact('title','Menus','Menu_Module'));

    }
}