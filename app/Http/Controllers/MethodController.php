<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Method;

class MethodController extends Controller {
    public function get_index() {
        $title = "Method";
        $data = Method::get();
        return view('admin.user_management.method.index_method', compact('title','data'));
    }
    public function get_create_method() {
        $title = "Create Method";
        return view('admin.user_management.method.create_method',compact('title'));
    }
    public function post_create_method(Request $request) {
        $data = [
            'name' =>$request->name,
            'display_name' => $request->display_name,
            'created_at' => date('Y-m-d h:m:s'),
        ];
        Method::insert($data);
        return Redirect()->to('method/index');
    }
    public function get_edit_method($id) {
        $title = "Edit Method";
        $data = Method::find($id);
        return view('admin.user_management.method.edit_method',compact('title','data'));
    }
    public function post_edit_method(Request $request, $id) {
        $data = [
            'name' =>$request->name,
            'display_name' => $request->display_name,
            'updated_at' => date('Y-m-d h:m:s')
        ];
        Method::where('id', '=', $id)->update($data);
        return Redirect()->to('method/index');
    }
    public function deleted($id) {
        $data = Method::where('id','=',$id)->delete();
        return Redirect()->to('method/index');
    }
}