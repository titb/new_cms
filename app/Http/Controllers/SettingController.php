<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function get_setting(){
        $title = "Setting";
        return view('admin.setting.form', compact('title'));
    }
}
