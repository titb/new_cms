<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Social;
class SocialController extends Controller
{
    public function index_social(){
        $title = "Social Meida";
        $social = Social::get();
        return view('admin.social.index',compact('title','social'));
    }
    public function index_social_json(){
        $data = Social::get();
        return response()->json($data);
    }
    public function create_social(Request $request){
        $title = "Create Social";
        $item_id = $request->item_id;
        return view('admin.social.form',compact('title','item_id'));
    }
    public function post_create_social(Request $request){
        if($request->hasFile('images')){
            $image = $request->file('images');
            $image_url = $this->move_image($image);
        }else{
            $image_url = $request->images1;
        }

        $data_social = [
            'name'=>$request->name,
            'class_icon'=>$request->icon_class,
            'images'=>$image_url,
            'status'=>$request->status
        ];
        if($request->item_id == 0){
            $social_id = Social::insertGetId($data_social);
            Social::where('id','=',$social_id)->update(['created_at'=>date('Y-m-d h:m:s')]);
            // Post::where('id','=',$post_id)->update(['created_at'=>date('Y-m-d h:m:s')]); 
        }else{
            
            $id = $request->item_id;
            $me = Social::find($id);
        //    $data_update = array_add($data_social);
           Social::where('id','=',$id)->update($data_social,['updated_at'=>date('Y-m-d h:m:s')]); 
        }
        return response()->json($data_social);
        // print_r($data);
    }
    public function social_get_json($id){
        $social_data = Social::find($id);
        return response()->json($social_data);
    }
    public function move_image($image){
        $url_np =  url('');
        $url_np = str_replace('/public','', $url_np);
        $im = $image;
        $fileName = $im->getClientOriginalName();
        $storage = date('M-Y');
        $destinationPath = storage_path('app/public/images/'.$storage) ;
            $up = $im->move($destinationPath,$fileName);
            $image_name = $destinationPath."/".$fileName;
            $path = $url_np."/storage/app/public/images/".$storage;
            $url =$path.'/'.$fileName;
            return $url;
    }
    public function deleted_social($id){
        $social = Social::find($id);
        $social->delete();
        return Redirect()->back();
    }
}
