<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Method;
use App\Role;
use App\Permission;
use DB;

class RoleController extends Controller {
    public function get_index() {
        $title = "Role";
        $data = Role::get();
        return view('admin.user_management.role.index_role', compact('title','data'));
    }
    public function get_create_role() {
        $title = "Create Role";
        $permissions = Permission::all();
        $groups = $permissions->groupBy('method_id');
        return view('admin.user_management.role.create_role',compact('title','groups'));
    }
    public function post_create_role(Request $request) {
        $data = [
            'name' =>$request->name,
            'display_name' => $request->display_name,
            'created_at' => date('Y-m-d h:m:s'),
        ];

        $role_id = Role::insertGetId($data);

        if($role_id > 0){
            foreach ($request->permission_id as $key => $value){
            $permiss = [
                'role_id'=>$role_id,
                'permission'=>$request->permission_id[$key]
            ];
            DB::table('role_permissions')->insert($permiss);
            }
        }
            return redirect()->to('role/index')->with('success','Successful Create');
    }
    public function get_edit_role(Request $request, $id) {
        $title = "Edit Role";
        $data = Role::find($id);
        $permissions = Permission::all();
        $groups = $permissions->groupBy('method_id');
        return view('admin.user_management.role.edit_role',compact('title','data','permissions','groups'));
    }
    public function post_edit_role(Request $request, $id) {
        $data = [
            'name' =>$request->name,
            'display_name' => $request->display_name,
            'updated_at' => date('Y-m-d h:m:s')
        ];
        Role::where('id', '=', $id)->update($data);

        if($request->permission_id) {
            $permis = DB::table('role_permissions')->where('role_id','=',$id)->delete();
            foreach ($request->permission_id as $key => $value){
            $permiss = [
                'role_id'=>$id,
                'permission'=>$request->permission_id[$key]
            ];

            DB::table('role_permissions')->insert($permiss);
        }
        
        return Redirect()->to('role/index');
    }
}
    public function deleted($id) {
        Role::where('id','=',$id)->delete();
        DB::table('role_permissions')->where('role_id','=',$id)->delete();
        return Redirect()->to('role/index');
    }
}