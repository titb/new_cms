<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Method;
use App\Permission;

class PermissionController extends Controller {
    public function get_index() {
        $title = "Permission";
        $data = Permission::get();
        return view('admin.user_management.permission.index_permission', compact('title','data'));
    }
    public function get_create_permission() {
        $title = "Create Method";
        $data = Method::get();
        return view('admin.user_management.permission.create_permission',compact('title','data'));
    }
    public function post_create_permission(Request $request) {
        $data = [
            'name' =>$request->name,
            'display_name' => $request->display_name,
            'method_id' => $request->method,
            'created_at' => date('Y-m-d h:m:s'),
        ];
        Permission::insert($data);
        return Redirect()->to('permission/index');
    }
    public function get_edit_permission($id) {
        $title = "Edit Permission";
        $data = Permission::find($id);
        $method = Method::get();
        return view('admin.user_management.permission.edit_permission',compact('title','data','method'));
    }
    public function post_edit_permission(Request $request, $id) {
        $data = [
            'name' =>$request->name,
            'display_name' => $request->display_name,
            'method_id' => $request->method,
            'updated_at' => date('Y-m-d h:m:s')
        ];
        Permission::where('id', '=', $id)->update($data);
        return Redirect()->to('permission/index');
    }
    public function deleted($id) {
        $data = Permission::where('id','=',$id)->delete();
        return Redirect()->to('permission/index');
    }
}