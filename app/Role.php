<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "rolse";
    public function permissions(){
        return $this->belongsToMany('App\Permission','role_permissions','role_id','permission');
    }
    public function hasanyPermission($permissions){
        if(is_array($permissions)){
            foreach($permissions as $val){
                if($this->hasPermission($val)){
                    return true;
                }
            }
        }
        else{
            if($this->hasPermission($permissions)){
                return true;
            }
        }
        return false;
    }
    public function hasPermission($permission){
        if($this->permissions()->where('name','=',$permission)->first()){
            return true;
        }
        return false;
    }
}
