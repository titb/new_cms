<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Method extends Model
{
    protected $table = "methods";

    public function permissions() {
        return $this->hasMany('App\Permission','method_id');
    }
    // public function shareholder(){
    //     return $this->belongsTo('App\Shareholder','shareholder_id');
    // }
}
