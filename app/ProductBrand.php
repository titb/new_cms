<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductBrand extends Model
{
    protected $table = "item_branchs";
    public function product_category(){
        return $this->belongsTo('App\ProductCategory','category');
    }
}