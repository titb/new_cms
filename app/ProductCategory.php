<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = "item_categorys";
    public function product_brand(){
        return $this->hasMany('App\ProductBrand','category');
    }
}