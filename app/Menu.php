<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table='menus';
    public function Modules()
    {
        return $this->belongsToMany('App\Module','menu_module','menu_id','module_id');
    }
}
