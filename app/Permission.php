<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = "permissions";

    public function methods() {
        return $this->belongsTo('App\Method','method_id');
    }
}
